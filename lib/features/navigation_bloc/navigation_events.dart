part of 'navigation_bloc.dart';

@immutable
abstract class IndexEvents{
  const IndexEvents();

}

class InitialIndexEvent extends IndexEvents{
  final int index =0;
}
class UpdateIndex extends IndexEvents {
  const UpdateIndex(this.index);

  final int index;

}