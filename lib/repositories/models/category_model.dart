class CategoryModel{

  final int id;
  final String name;
  final String image;

  toString(){
    return 'id $id, name $name';
  }

  CategoryModel({
    this.id,
    this.name,
    this.image,
  }
      );
  factory CategoryModel.fromJson(Map<String, dynamic> json){
    return CategoryModel(
      id: json['id'] as int,
      name: json['name'] as String,
      image: json['description'] as String,
    );
  }
  String getName(){
    return name;
  }

}