class AddressModel{
  int id;
  String homeNumber;
  String street;
  String country;
  String city;
  String flatNumber;
  String floor;
  bool check;

  AddressModel({
    this.id,
    this.country,
    this.homeNumber,
    this.city,
    this.flatNumber,
    this.street,
    this.floor,
    this.check
  });

  factory AddressModel.fromJson(Map<String, dynamic> json){
    return AddressModel(
      id: json['id'] as int,
      country: json ['country'] as String,
      homeNumber: json['home_number'] as String,
      city: json['city'] as String,
      flatNumber: json['flat_number'] as String,
      street: json['street'] as String,
      floor: json['stage'] as String,
      check: json['check']==1? true: false,
    );
  }
  String toString(){
    return 'г.${this.city}, ул. ${this.street}, ${this.homeNumber}';
  }
}

class CompanyAddress{
  int id;
  String address;
  double longitude;
  double latitude;


  CompanyAddress({
    this.id,
    this.longitude,
    this.address,
    this.latitude
  });

  factory CompanyAddress.fromJson(Map<String, dynamic> json){
    return CompanyAddress(
      id: json['id'] as int,
      address: json ['address'] as String,
      latitude: double.parse(json['first_cord']),
      longitude: double.parse(json['second_cord'] ),
    );
  }
}


class CompanyContact{
  int id;
  String phoneNumber;
  CompanyContact({
    this.id,
    this.phoneNumber,
 
  });

  factory CompanyContact.fromJson(Map<String, dynamic> json){
    return CompanyContact(
      id: json['id'] as int,
      phoneNumber: json ['phone_number'] as String,
    );
  }

}