
class CardModel{
  int id;
  String tokenCard;
  bool check;
  String cardFirstSix;
  String cardLastFour;
  CardModel({
    this.id,
    this.tokenCard,
    this.check,
    this.cardFirstSix,
    this.cardLastFour
  });

  factory CardModel.fromJson(Map<String, dynamic> json){
    return CardModel(
        id: json['id'],
        tokenCard: json['tokenCard'],
        check: json['check']==0?false:true,
        cardFirstSix: json['CardFirstSix'],
        cardLastFour: json['CardLastFour']
    );
  }
}