class SlideModel{
  final int id;
  final String image;
  final String text;
  final String link;

  SlideModel({this.id,this.image,this.text,this.link});
  factory SlideModel.fromJson(Map<String, dynamic> json){
    return SlideModel(
      id: json['id'] as int,
      image: json['image'] as String,
      text: json['text'] as String,
      link: json['link'] as String,
    );
  }
}