import 'dart:collection';

class Country{
  final int id;
  final String name;
  final List<City> cities;
  Country({
    this.id,
    this.name,
    this.cities
  });


  factory Country.fromJson(Map<String, dynamic> json){
      return Country(
        id: json['id'],
        name: json['name'],
        cities: json['cities'].cast<Map<String, dynamic>>().map<City>((json) => City.fromJson(json)).toList(),
      );
  }
  String toString(){
    return id.toString() + name + cities.toString();
  }
}

class City{
  final int id;
  final String name;
  final dynamic countryId;

  City({
    this.id,
    this.name,
    this.countryId
  });
  factory City.fromJson(Map<String, dynamic> json){
    return City(
        id: json['id'],
        name: json['city'],
        countryId: json['country_id'],
    );
  }
  String toString(){
    return id.toString() + name;
  }
}