class Article{
  int id;
  final String name;
  final String image;
  final String description;

  Article(
  {this.id,this.image,this.name,this.description});

  factory Article.fromJson(Map<String, dynamic> json){
    return Article(
      id: json['id'] as int,
      image: json['image'] as String,
      name: json['name'] as String,
      description: json['description'] as String,
    );
  }
}