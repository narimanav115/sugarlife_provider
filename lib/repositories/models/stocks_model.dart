import 'dart:collection';

import 'package:sugarlife/repositories/models/product_model.dart';

class Stock{
  int id;
  String name;
  String image;
  List<Product> products;
  Stock({this.id,this.name,this.image, this.products});
  factory Stock.fromJson(Map<String, dynamic> json){
    return Stock(
      id: json['id'] as int,
      name: json['name'] as String,
      image: json['image'] as String,
      products: json['items']
        .map<Product>((json) =>
        Product.fromJson(json))
        .toList(),
    );
  }
}