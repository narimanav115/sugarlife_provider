
class UserModel {
  final int id;
  final String name;
  final String surname;
  final String email;
  final String phone;
  String password;
  UserModel({
    this.id,
    this.name,
    this.surname,
    this.email,
    this.password,
    this.phone
  });

  factory UserModel.fromJson(Map<String, dynamic> json){
    return UserModel(
      id: json['id'] as int,
      name: json['name'] as String,
      surname: json['surname'] as String,
      email: json['email'] as String,
      phone: json["number"] as String,
    );
  }
}