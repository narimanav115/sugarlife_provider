import 'dart:collection';
import 'dart:convert';


class Product{

  final dynamic id;
  final dynamic name;
  final dynamic description;
  final dynamic price;
  final dynamic image;
  final dynamic discountPrice;
  final dynamic composition;
  final dynamic usage;
  final dynamic categoryId;
  final dynamic weight;
  final String measureType;
  bool isFavourite;
  bool inCart;
  final bool isAvailable;
  Product({
    this.measureType,
    this.id,
    this.name,
    this.description,
    this.price,
    this.image,
    this.discountPrice,
    this.categoryId,
    this.isFavourite = false,
    this.inCart = false,
    this.isAvailable,
    this.composition,
    this.usage,
    this.weight,
  }
      );
  factory Product.fromJson(Map<String, dynamic> json){
        // print(json);
        var id = json['favorite'].toString().split('').contains('i');
        // print(json['favorite']);
        return Product(
          id: json['id'],
          name: json['name'] as String,
          description: json['description'] as String,
          price: json['price'] ,
          discountPrice: json['discount_price'] ,
          image: json['image'] as String,
          usage: json['usage'],
          weight: json['massa'],
          measureType: json['check_type']==1?'г':'мл',
          isFavourite: id,
          composition: json['composition'],
          isAvailable:  json['is_available']==1? true: false,
          categoryId: json['category_id'],
        );

  }

  void setFavorite(){
    this.isFavourite = true;
  }
}
class CartItemModel{
  int orderId;
  int id;
  dynamic weight;
  final String imageUrl;
  int quantity;
  String price;
  String itemName;
  final String measureType;

  CartItemModel({
    this.measureType,
    this.weight,this.orderId,this.imageUrl, this.id,this.quantity,this.price='0',this.itemName});
  Map<String, dynamic> toJson() =>{
    "item_id":id,
    "quantity": quantity,
  };
  factory CartItemModel.fromJson(Map<String,dynamic> json){
    return CartItemModel(
      measureType: json['check_type']==1?'г':'мл',
      id: json['id'],
      weight: json['massa'],
      itemName: json['item_name'],
      price: json['price'],
      imageUrl: json['item_image'],
      quantity: json['quantity'],
    );
  }
  String toString(){
    return "{'item_id':$id,'quantity': $quantity},";
  }
}

class OrderModel{
  int id;
  String positions;
  List<CartItemModel> cartItemModel;
  double price;
  String totalPrice;
  int deliveryPrice;
  String status;
  String creationDate;
  OrderModel(
        {
          this.id,
          this.positions,
          this.price,
          this.deliveryPrice,
          this.creationDate,
          this.cartItemModel,
          this.status,
          this.totalPrice
        }
      );

  Map<String, dynamic> toJson() =>{
    'items': cartItemModel,
    'total_sum': price,
    'delivery_price': deliveryPrice
  };
  factory OrderModel.fromJson(Map<String, dynamic> json){
    return OrderModel(
      id: json['id'] as int,
      positions: json['count'] as String,
      totalPrice: json['total_price'] as String,
      creationDate: json['created_at'],
      status: json['status']['name'],
    );
  }
  String toString(){
    return '${this.toJson()}';
  }
}