import 'dart:async';
import 'dart:collection';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sugarlife/repositories/controllers/const.dart';
import 'package:sugarlife/repositories/controllers/repos.dart';
import 'package:sugarlife/repositories/controllers/session.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/repositories/models/card_model.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/repositories/models/user_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserController with ChangeNotifier{
  final Repository repository;
  FlutterSecureStorage storage;
  String lastOrderId = '';
  String lastOrderSum = '';
  // Session session;
  String body;
  List<OrderModel> ordersHistory = [];
  List<CartItemModel> clickedOrder = [];
  Map<int ,List<CartItemModel>> allClickedOrders = {};
  Map json;
  UserController({this.repository,this.storage}){
    getTokenFromStorage();
  }
  void getTokenFromStorage() async{
    // repository.isAuthenticated = false;
    repository.token = await storage.read(key:"token");
    if(repository.token!=null){
      //TODO remove this shit
      print(repository.token);
      repository.isAuthenticated = true;
      repository.session.addToken(repository.token);
      // repository.headers['Authorization'] = 'Bearer ${repository.token}';
      getAllUserData();
      getOrdersHistory();
    }
  }
  void getAllUserData(){
    getProfile();
    getFavorites();
    getAddresses();
    getCards();

    notifyListeners();
  }
  bool getAuthenticated(){
    return repository.isAuthenticated;
  }
  Future<bool> registration(UserModel user) async{
    try{
      body = jsonEncode(<String, dynamic>{
        'name': user.name,
        'email': user.email,
        'password': user.password,
        'surname': user.surname,
        'number': user.phone
      });
      
      http.Response response = await  repository.session.post(Uri.parse(baseUrl+'register'), body);

      if(response.statusCode == 200){
        if(jsonDecode(response.body)['data']['token']!=null) {
          repository.token = jsonDecode(response.body)['data']['token'];
          repository.session.addToken(repository.token);
          storage.write(key:'token', value: repository.token);
          repository.isAuthenticated = true;

          getProfile();

          notifyListeners();
          return true;
        }else{
          print(response.statusCode);
          print('screwed up');
          return false;
        }
      }else{

        print(response.statusCode);
        return false;
      }

      // Future.delayed(Duration(seconds: 3));
      // repository.user = User(id: 1,name: name,surname: 'Testov', email: email ,password: password );
      // notifyListeners();
    }catch(e){
      print(e);
    }
    return false;
  }
  void getProfile() async{
    try{
      http.Response response = await  repository.session.get(Uri.parse(baseUrl+'profile'));
      if(response.statusCode == 200) {
        repository.user = UserModel.fromJson(jsonDecode(response.body));
        repository.isAuthenticated = true;
      }else{
        print('screwed up');
      }
    }catch(e){
      print(e);
    }
  }
  Future<bool> authenticate(String email, String password) async{
    repository.authError = '';
    var res = false;
    try{
      body = jsonEncode(<String, String>{
        'email': email,
        'password': password,
      });
      http.Response response = await repository.session.post(Uri.parse(baseUrl+'login'), body);
      if(jsonDecode(response.body)['user']!=null) {
        repository.user = UserModel.fromJson(jsonDecode(response.body)['user']);
        repository.token = jsonDecode(response.body)['token'];
        if(repository.token!=null){
          repository.session.addToken(repository.token);
          storage.write(key:'token', value: repository.token);
          repository.isAuthenticated = true;
          repository.authError = '';
          res = true;
          getAllUserData();
        }else{
          repository.authError = response.body;
          res = false;
          print('screwed up');
        }
      }else{
        repository.authError = response.body;
        print('screwed up');
      }
      notifyListeners();
      // Future.delayed(Duration(seconds: 3));
      // repository.user = User(id: 1,name: name,surname: 'Testov', email: email ,password: password );
      // notifyListeners();
    }catch(e){
      print(e);
    }
    return res;
  }
  void authByEmail(String email, String password){
    Future.delayed(Duration(seconds: 3));
    if(email == repository.tempUser.email && password == repository.tempUser.password){
      repository.isAuthenticated = true;
      notifyListeners();
    }else{

      print('lol');
    }
  }
  Future<bool> changeUserData({String name, String surname, String email, String phone}) async{
    var res = false;
    try{
      body = jsonEncode(<String, String>{
        'email': email,
        'surname': surname,
        'name': name,
        'number': phone,
      });
      http.Response response = await repository.session.put(Uri.parse(baseUrl+'profile/update'), body);
      if(response.statusCode==204||response.statusCode==200){
        res = true;
        getProfile();
        notifyListeners();
      }else{
        res = false;
      }
    }catch(e){
      print(e);
    }
    return res;
  }
  Future<Map<String,dynamic>> changePassword({String oldPassword, String newPassword, String conNewPassword}) async{
    var res = false;
    try{
      body = jsonEncode(<String, String>{
        'old_password': oldPassword,
        'new_password': newPassword,
        'conf_password': conNewPassword,
      });
      http.Response response = await repository.session.put(Uri.parse(baseUrl+'profile/change-password'), body);
      print(response.body);
      print(response.statusCode);

      if(response.statusCode==200){
        if(response.body == 'Password changed'){
          return {'status':true,};
        }
        }else if(response.statusCode==418){
        if(jsonDecode(response.body)['status'] == 'Password error'){
          return {
            'status':false,
            'error': 'Проверьте правильность введенных паролей'
          };
        }
        else if(jsonDecode(response.body)['status'] == 'Password is wrong'){
          return {
            'status':false,
            'error': 'Неправильно введен текущий пароль'
          };
        }
        else if(jsonDecode(response.body)['status'] == 'Password Incorrect'){
          return {
            'status':false,
            'error': 'Пароли не совпадают'
          };
        }

        }else if(response.statusCode == 422){
          return {'status':false, 'error': 'Проверьте заполненность полей'};
        }else{
          return {'status':false, 'error': 'Ошибка сервера'};
        }
    }catch(e){
      print(e);
    }
    return {'status':false, 'error': 'Ошибка сервера'};
  }

  void getAddresses() async{
    try{
      http.Response response = await repository.session.get(Uri.parse(baseUrl+'addresses'));
      if(response.statusCode == 200){
        repository.addresses = jsonDecode(response.body)['address']
            .cast<LinkedHashMap<String, dynamic>>()
            .map<AddressModel>((json) =>
            AddressModel.fromJson(json))
            .toList();
      }else{
        print('screwed up');
      }

    }catch(e){
      print(e);
    }
  }
  Future <bool> deleteAddress({int id})async{
    try{
      body = jsonEncode(<String, String>{
      });
      http.Response response = await http.delete(Uri.parse(baseUrl+'address/$id/delete'),headers: repository.session.headers);
      if(response.statusCode == 200){
        repository.addresses.removeWhere((element) => element.id ==id);
        notifyListeners();
        return true;
      }else{
        print(response.body);
        print('screwed up');
      }
    }catch(e){
      print(e);
    }
    return false;
  }
  void checkAddress(int id) async{
    repository.addresses.where((element) => element.id != id?element.check=false:element.check=true);
    try{
      http.Response response = await repository.session.putWithout(Uri.parse(baseUrl+'address/checked/$id'),);
      if(response.statusCode == 200){
        print('ok');

      }else{
        print('screwed up');
      }
      notifyListeners();
      // getAllUserData();
    }catch(e){
      print(e);
    }
  }
  Future<bool> updateAddress({int id, String flatNumber,String homeNumber,String street,String city,String floor, String country}) async{
    var res = false;
    body = jsonEncode(<String, String>{
      'flat_number': flatNumber.toString(),
      'home_number': homeNumber,
      'stage' : floor.toString(),
      'street' : street,
      'city' : city,
      'country': country
    });
    try{
      http.Response response = await repository.session.put(Uri.parse(baseUrl+'address/$id/update'), body);
      if(response.statusCode == 200||response.statusCode == 204){
        var tempUser = repository.addresses.firstWhere((element) => element.id==id);
        tempUser.floor =  floor;
        tempUser.city = city;
        tempUser.homeNumber = homeNumber;
        tempUser.street = street;
        tempUser.flatNumber = flatNumber;
        tempUser.country = country;
        repository.addresses[repository.addresses.indexWhere((element) => element.id ==id)] = tempUser;
        print(repository.addresses[repository.addresses.indexWhere((element) => element.id ==id)]);
        notifyListeners();
        res = true;
        print('ok');
      }else{
        print(response.statusCode);
        print(response.body);
        print('screwed up');
      }
      getAddresses();
      notifyListeners();
    }catch(e){
      print(e);
    }
    return res;
  }
  Future<bool> addAddress({String flatNumber,String homeNumber,String street,String city,String floor,String country}) async{
    var res = false;
    body = jsonEncode(<String, String>{
      'flat_number': flatNumber,
      'home_number': homeNumber,
      'stage' : floor,
      'street' : street,
      'city' : city,
      'country': country,
    });
    try{
      http.Response response = await repository.session.post(Uri.parse(baseUrl+'address/store'), body);
      if(response.statusCode == 200){
        res = true;
        repository.addresses.add(AddressModel.fromJson(jsonDecode(response.body)['address']));
        notifyListeners();
        print('ok');
      }else{
        print(response.body);
        print('screwed up');
      }
    }catch(e){
      print(e);
    }
    return res;
  }
  void getFavorites() async{
    try{
      http.Response response = await repository.session.get(Uri.parse(baseUrl+'favorite'));
      if(response.statusCode == 200){
        repository.favorites = jsonDecode(response.body).cast<LinkedHashMap<String, dynamic>>().map<Product>((json) =>
            Product.fromJson(json['items']))
            .toList();
        repository.favorites.forEach((element) {element.setFavorite();});

        notifyListeners();

      }else{
        print('screwed up');
      }
    }catch(e){
      print(e);
    }
  }

  void unAuthorize() async{
    repository.isAuthenticated = false;
    try{
      storage.delete(key: "token");
      repository.token = "";
      http.Response response = await http.post(Uri.parse(baseUrl+'logout'), headers: repository.session.headers);
      if(jsonDecode(response.body)['message']!=null) {
        repository.isAuthenticated = false;
        repository.favorites =[];
        repository.session = null;
        storage.delete(key: "token");
        repository.token = "";
        repository.user = null;
        print(jsonDecode(response.body)['message']);
      }
    }catch(e){
      print(e);
    }
    notifyListeners();
  }
  void updateFavorite(int item) async{
    if(repository.favorites.where((element) => element.id==item).isNotEmpty){
      repository.favorites.removeWhere((element) => element.id==item);
      repository.productList.firstWhere((element) => element.id==item).isFavourite = false;
    }else{
      repository.favorites.add(repository.productList.firstWhere((element) => element.id == item));
      repository.productList.firstWhere((element) => element.id==item).isFavourite = true;
    }
     try {
      http.Response response = await repository.session.postWithout(Uri.parse(baseUrl + 'favorite/$item'));
      if(response.statusCode == 200|| response.statusCode ==204){
        print('ok');
      }else{
        print(response.statusCode);
        print('screwed up');
      }
    }catch(e){
      print(e);
    }
    notifyListeners();
  }

  Future<bool> resetPassword(String email) async{
    repository.authError = '';
    var res = false;
    try{
      body = jsonEncode(<String, String>{
        'email': email,
      });
      http.Response response = await repository.session.post(Uri.parse(baseUrl+'reset-password'), body);
      if(response.statusCode ==200) {
        return true;
      }else{
        print(response.statusCode);
        print(response.body);
        return false;
      }
    }catch(e){
      print(e);
    }
    return res;
  }

  Future<bool> feedback(String email, String topic, String desc) async{
    if(email!=''&&topic!=''&&desc!=''){
      try{
        body = jsonEncode(<String, String>{
          'email': email,
          'name':topic,
          'content':desc,
        });
        http.Response response = await repository.session.post(Uri.parse(baseUrl+'contact-us'), body);
        if(response.statusCode ==200) {
          return true;
        }else{
          print(response.statusCode);
          print(response.body);
        }
      }catch(e){
        print(e);
      }
    }
    return false;
  }
  void getOrdersHistory() async{
    try{
      http.Response response = await repository.session.get(Uri.parse(baseUrl+'order/history'));
      if(response.statusCode == 200){
        ordersHistory = jsonDecode(response.body)['order'].cast<LinkedHashMap<String, dynamic>>().map<OrderModel>((json) =>
            OrderModel.fromJson(json))
            .toList();
      }else{
        print('orders error');
        print('screwed up:' + response.statusCode.toString() +'\n${response.body}');
      }
    }catch(e){
      print(e);
    }

  }
  void getOrder(int id) async{
    clickedOrder = [];
    try{
      http.Response response = await repository.session.get(Uri.parse(baseUrl+'order/$id/show'));
      if(response.statusCode == 200){
        clickedOrder =  jsonDecode(response.body)['orders'].cast<LinkedHashMap<String, dynamic>>().map<CartItemModel>((json) =>
            CartItemModel.fromJson(json))
            .toList();
        allClickedOrders[id] = clickedOrder;
      }else{
        print('screwed up:' + response.statusCode.toString() +'\n${response.body}');
      }
    }catch(e){
      print(e);
    }
    notifyListeners();
  }
  Future<bool> calculateDelivery() async {
    var body = '';
    if(repository.isAuthenticated){
      body = jsonEncode(<String, dynamic>{
        'items': repository.cartItemsList,
        'total_sum': 0,
        'delivery_price': 0,
        'delivery_status': repository.deliveryOption?1:0,
        'pay_status': 0,
      });
    }else{
      if(repository.tempAddress==null){
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status': repository.deliveryOption?1:0,
          'pay_status': 0,
        });
      }else{
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status': repository.deliveryOption?1:0,
          'pay_status': 0,
          'city': repository.tempAddress==null?'Alamty':repository.tempAddress.city,
          'country': repository.tempAddress==null?'Казахстан':repository.tempAddress.country,
        });
      }
    }
    print(body);
    try {
      http.Response response = await repository.session.post(
          Uri.parse(baseUrl + 'calc'), body);
     if (response.statusCode == 200){
       repository.calculatedSum = jsonDecode(response.body)['total_sum'];
       if(repository.deliveryOption)repository.deliveryPrice = jsonDecode(response.body)['delivery_price'];
       notifyListeners();
       return true;
     }else{
       print(jsonDecode(response.body));
     }

    } catch (e) {
      print(e);
    }
    return false;
  }
  Future<bool> finalOrder() async{

    var body = '';
    if(repository.selectedCard!=null){
      if(repository.deliveryOption){
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status':1,
          'pay_status':0,
          'card_id': repository.selectedCard.id,
        });
      }else{
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status':0,
          'data_delivery': repository.pickupDate,
          'time_delivery': repository.pickupTime,
          'pay_status':0,
          'card_id': repository.selectedCard.id,
        });
      }
    }else{
      if(repository.deliveryOption){
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status':1,
          'pay_status':0,
        });
      }else{
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status':0,
          'data_delivery': repository.pickupDate,
          'time_delivery': repository.pickupTime,
          'pay_status':0,
        });
      }
    }
    print(body);
    try{
      http.Response response = await repository.session.post(Uri.parse(baseUrl+'order/store'),body);
      if(response.statusCode == 200){
        print(response.body);
        repository.cartItemsList =[];
        repository.cartItems ={};
        repository.cart = [];
        repository.cartMap = {};
        repository.totalSum = 0;
        repository.totalItems = 0;
        getOrdersHistory();
        lastOrderSum = jsonDecode(response.body)['order']['total_price'].toString();
        lastOrderId = jsonDecode(response.body)['order']['id'].toString();
        print(jsonDecode(response.body)['order']['id']);
        notifyListeners();
        return true;
      }else{
        print(response.body);
      }
    }catch(e){
      print(e);
    }
    notifyListeners();
    return false;
  }

  Future<bool> deleteCard(int cardId) async{
    http.Response response = await http.delete(Uri.parse(baseUrl+'delete-card/$cardId'),headers: repository.session.headers);
    if(response.statusCode == 200){
      repository.cards.removeWhere((element) => element.id==cardId);
      notifyListeners();
      return true;
    }else{
      print(response.body);
      return false;
    }
  }

  //pay
  Future<bool> payToken() async {
    body = jsonEncode(<String, dynamic>{
      'items': repository.cartItemsList,
      'amount': lastOrderSum,
      'delivery_price': 0,
      'delivery_status':repository.deliveryOption?1:0,
      'pay_status':0,
      'card_id': repository.selectedCard.id,
      'order_id': lastOrderId,
    });
    http.Response payResponse = await repository.session.post(Uri.parse(baseUrl+'pay'),body);
    if(payResponse.statusCode==200){
      return true;
    }else{
      print(payResponse.body);
    }
    return false;
  }
  Future<Map<String, dynamic>> addCard({String crypto, String name, String url, bool save}) async {
    String body;
    if(save){
      body = jsonEncode(<String, dynamic>{
      'cryptogram': crypto,
      'name': name,
      });
    }else{
      body = jsonEncode(<String, dynamic>{
        'cryptogram': crypto,
        'name': name,
        'amount': repository.calculatedSum,
        // 'order_id': lastOrderId,
      });
    }
    try {
      http.Response response = await repository.session.post(
          Uri.parse(baseUrl + 'save-card?$url'), body);
      if (response.statusCode == 302) {
        print(response.body);
        return jsonDecode(response.body);
       }else if(response.statusCode == 200){
        print('200');
        print(response.body);
        return {
          'success': true,
          'code':0,
        };
      }else{
        print(response.body);
        return {
          'success': 'success',
          'status': 500,
        };
      }
      } catch (e) {
      print(e);
    }
    return {
      'success': 'loh',
    };
  }

  void getCards() async{
    try{
      http.Response response = await repository.session.get(Uri.parse(baseUrl+'cards'));
      if(response.statusCode == 200){
        repository.cards = jsonDecode(response.body)['cards']
            .cast<LinkedHashMap<String, dynamic>>()
            .map<CardModel>((json) =>
            CardModel.fromJson(json))
            .toList();
        notifyListeners();
      }else{
        print('screwed up');
      }

    }catch(e){
      print(e);
    }
  }

  String getUserId(){
    return repository.user.id.toString();
  }

  selectCard(int id){
    repository.selectedCard = repository.cards.firstWhere((element) => element.id == id);
    notifyListeners();
  }
  Future<bool> orderWithoutAuth() async{
        var body = '';
        if(repository.deliveryOption){
          body = jsonEncode(<String, dynamic>{
            "items": repository.cartItemsList,
            "total_sum": 0,
            "delivery_price": 0,
            "delivery_status": 1,
            "pay_status": 0,
            "country": repository.tempAddress.country,
            "fullName": repository.tempUser.name,
            "phone_number": repository.tempUser.phone,
            "email": repository.tempUser.email,
            "home_number": repository.tempAddress.homeNumber,
            "street": repository.tempAddress.street,
            "stage": repository.tempAddress.floor,
            "flat_number": repository.tempAddress.flatNumber,
            "city": repository.tempAddress.city
          });
        }else{
          body = jsonEncode(<String, dynamic>{
            "items": repository.cartItemsList,
            "total_sum": 0,
            "delivery_price": 0,
            "delivery_status": 0,
            "pay_status": 0,
            "fullName": repository.tempUser.name,
            "phone_number": repository.tempUser.phone,
            "email": repository.tempUser.email,
            'data_delivery': repository.pickupDate,
            'time_delivery': repository.pickupTime,
          });
        }
        try{
          http.Response response = await repository.session.post(Uri.parse(baseUrl+'order/store'),body);
          if(response.statusCode == 200){
            print(response.body);
            repository.cartItemsList =[];
            repository.cartItems ={};
            repository.cart = [];
            repository.cartMap = {};
            repository.totalSum = 0;
            repository.totalItems = 0;
            lastOrderSum = jsonDecode(response.body)['order']['total_price'].toString();
            lastOrderId = jsonDecode(response.body)['order']['id'].toString();
            print(jsonDecode(response.body)['order']['id']);
            notifyListeners();
            return true;
          }else{
            print(response.body);
            return false;
          }
        }catch(e){
          print(e);
        }
        return false;
  }


}