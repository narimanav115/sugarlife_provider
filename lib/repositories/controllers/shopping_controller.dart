import 'dart:collection';
import 'package:category_picker/category_picker_item.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sugarlife/repositories/controllers/repos.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/repositories/models/article_model.dart';
import 'package:sugarlife/repositories/models/category_model.dart';
import 'package:sugarlife/repositories/models/city_model.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:http/http.dart' as http;
import 'package:sugarlife/repositories/models/slide_model.dart';
import 'package:sugarlife/repositories/models/stocks_model.dart';
import 'dart:convert';
import 'const.dart';

class ShoppingController with ChangeNotifier{

  var res = '';
  var cert = '';
  var conf = '';
  int index = 1;
  var clickedOrders = [];
  List<String> countryNames = [];
  Product temp;
  String token = '';
  final Repository repository;
  FlutterSecureStorage storage;
  ShoppingController({this.repository, this.storage}){
    getTokenFromStorage();
  }

  void getTokenFromStorage() async{
    // repository.isAuthenticated = false;
    repository.token = await storage.read(key:"token");
    if(repository.token!=null){
      repository.headers['Authorization'] = 'Bearer ${repository.token}';
    }else{
      print('token loh');
    }
    getAboutUs();
    getAllCities();
    getMainPageData();
  }
  void selectProduct(Product product){
    repository.selectedProd = product;
    repository.selectedIsUploaded = true;
    notifyListeners();
  }
  void getProducts(int id) async{
    repository.productList = [];
    http.Response response = await http.get(Uri.parse(baseUrl+'categories/$id/items'),headers: repository.headers);
    // repository.mainPageProds.forEach((element) {if(element.categoryId == id)repository.productList.add(element);});
    // notifyListeners();
    try{
      if(response.statusCode ==200){
        repository.productList = jsonDecode(response.body)['items']['data'].cast<LinkedHashMap<String, dynamic>>()
            .map<Product>((json) =>
            Product.fromJson(json))
            .toList();
        repository.selectedIsUploaded = true;
        notifyListeners();
      }
    }catch(e){
      print(e);
      print('issue getproducts');
    }
  }
  Future<bool> getProductById(int productId, int categoryId) async{
    try{
      http.Response response = await http.get(Uri.parse(baseUrl+'categories/$categoryId/items/$productId'),headers: repository.headers);
      if(response.statusCode == 200){
        final parsed = jsonDecode(response.body)['item'];
        selectProduct(Product.fromJson(parsed));
        return true;
      }
    }catch(e){
      print(e);
    }
    return false;
  }
  void getAllProducts() async{
    try{
      repository.selectedIsUploaded = false;
      http.Response response = await http.get(Uri.parse(baseUrl+'main-page'),headers: repository.headers);
      if(response.statusCode ==200){
        repository.productList = jsonDecode(response.body)['data']['items']['data'].cast<LinkedHashMap<String, dynamic>>()
            .map<Product>((json) =>
            Product.fromJson(json))
            .toList();
        repository.selectedIsUploaded = true;
        notifyListeners();
      }
    }catch(e){
      print(e);
    }
  }
  void sort({String property, int direction}) async{
    try{
      repository.selectedIsUploaded = false;
      http.Response response = await http.get(Uri.parse(baseUrl+'filter/$direction/$property'),headers: repository.headers);
      if(response.statusCode == 200){
        repository.productList = jsonDecode(response.body)['items']['data'].cast<LinkedHashMap<String, dynamic>>()
            .map<Product>((json) =>
            Product.fromJson(json))
            .toList();
        repository.selectedIsUploaded = true;
        notifyListeners();
      }
    }catch(e){
      print(e);
      print('sort issue');
    }
  }
  addSortProducts({int direction,String property, int page}) async{
    // print('$property + $direction + $page');
    List<Product> temp = [];
    try{
     http.Response response = await http.get(Uri.parse(baseUrl+'filter/$direction/$property?page=$page'),headers: repository.headers);
      if(response.statusCode == 200) {
        temp = jsonDecode(response.body)['items']['data'].cast<
            LinkedHashMap<String, dynamic>>()
            .map<Product>((json) =>
            Product.fromJson(json))
            .toList();
        repository.productList = repository.productList + temp;
      }else{
        print(response.statusCode);
      }
    }catch(e){
      print('sort add issue'+e);
    }
    notifyListeners();
  }
  addProducts({int page, int categoryId,}) async{
    try{
      http.Response response;
      List<Product> temp = [];
      if(categoryId>0){
       response = await http.get(Uri.parse(baseUrl+'categories/$categoryId/items?page=$page'),headers: repository.headers);
       if(response.statusCode == 200){
         temp =  jsonDecode(response.body)['items']['data'].cast<LinkedHashMap<String, dynamic>>()
             .map<Product>((json) =>
             Product.fromJson(json))
             .toList();
         repository.productList = repository.productList + temp;
       }else{
         print(response.statusCode);
         print('new items load error');
       }
      }else{
        response = await http.get(Uri.parse(baseUrl+'main-page?page=$page'),headers: repository.headers);
        if(response.statusCode == 200){
          temp =  jsonDecode(response.body)['data']['items']['data'].cast<LinkedHashMap<String, dynamic>>()
              .map<Product>((json) =>
              Product.fromJson(json))
              .toList();
          repository.productList = repository.productList + temp;
        }else{
          print(response.statusCode);
          print('new items load error');
        }
      }

    }catch(e){
      print('pagination issue'+e);
    }
    notifyListeners();
  }
  void getMainPageData() async{
    try{
      repository.selectedIsUploaded = false;
      http.Response response = await http.get(Uri.parse(baseUrl+'main-page'),headers: repository.headers);
      if(response.statusCode == 200){

        repository.categoriesList = jsonDecode(response.body)['data']['categories'].cast<LinkedHashMap<String, dynamic>>()
            .map<CategoryModel>((json) =>
            CategoryModel.fromJson(json))
            .toList();
        generateCategoryWidgets();
        print('cats');
        repository.slides = jsonDecode(response.body)['data']['slides'].cast<LinkedHashMap<String, dynamic>>()
            .map<SlideModel>((json) =>
            SlideModel.fromJson(json))
            .toList();
        print('slide');
        repository.productList = jsonDecode(response.body)['data']['items']['data'].cast<LinkedHashMap<String, dynamic>>()
            .map<Product>((json) =>
            Product.fromJson(json))
            .toList();
        print('main page prods');
        //КОСТЫЛИЩЕ
        if (repository.productList.isNotEmpty) {repository.selectedIsUploaded = true;}
        repository.articles = jsonDecode(response.body)['data']['articles'].cast<LinkedHashMap<String, dynamic>>()
            .map<Article>((json) =>
            Article.fromJson(json))
            .toList();
        print('articles');
        repository.stocks = jsonDecode(response.body)['data']['stocks'].cast<LinkedHashMap<String, dynamic>>()
            .map<Stock>((json) =>
            Stock.fromJson(json))
            .toList();
        print('stocks');
        repository.companyContacts = jsonDecode(response.body)['data']['contacts'].cast<LinkedHashMap<String, dynamic>>()
            .map<CompanyContact>((json) =>
            CompanyContact.fromJson(json))
            .toList();
        print('company contacts');
        repository.companyAddresses = jsonDecode(response.body)['data']['addresses'].cast<LinkedHashMap<String, dynamic>>()
            .map<CompanyAddress>((json) =>
            CompanyAddress.fromJson(json))
            .toList();
        print('company addresses');
        repository.companyAddresses.forEach((element) {
          repository.markers.add(Marker(markerId: MarkerId('${element.id}'), position: LatLng(element.latitude, element.longitude)));
        });
        // ;

      }
    }catch(e){
      print(e);
    }
  }
  void getCategories() async{
    try{
      http.Response response = await http.get(Uri.parse(baseUrl+'categories/'));
      if(response.statusCode == 200){
        final parsed = jsonDecode(response.body)['categories'].cast<LinkedHashMap<String, dynamic>>();
        repository.categoriesList = parsed.map<CategoryModel>((json) => CategoryModel.fromJson(json)).toList();
        notifyListeners();
      }
    }catch(e){
      print(e);
    }
  }

  void addToCart(Product product){
    repository.totalItems+=1;
    if(repository.cartMap.keys.toList().where((element) => product.id == element.id).isNotEmpty){
      temp = repository.cartMap.keys.toList().singleWhere((element) => element.id ==product.id);
      repository.cartMap[temp] +=1;
      repository.cartItemsList.firstWhere((element) => element.id == product.id).quantity +=1;
    }else{
      repository.cartMap[product] = 1;
      repository.cartItemsList.add(CartItemModel(id: product.id, quantity: 1));
    }
    repository.totalSum += product.discountPrice!=null?product.discountPrice:product.price;
    repository.cartItems[product.id]!=null
        ?repository.cartItems[product.id] += 1
        :repository.cartItems[product.id] = 1;
    // refreshCartMap();
    calculateDelivery();
    notifyListeners();
    // finalOrder();
  }
  void removeFromCart(Product product){
    if (repository.cartMap[product]!=null)
      repository.cartMap[product] -=1;
      repository.cartItems[product.id] -= 1;
      repository.cartItemsList.firstWhere((element) => element.id == product.id).quantity -=1;
    if(repository.cartMap[product]==0)
      removeUpdate(product);
    repository.totalItems-=1;
    repository.totalSum -= product.discountPrice!=null?product.discountPrice:product.price;
    notifyListeners();
  }
  void removeUpdate(Product product){
    repository.cartMap.remove(product);
    repository.cartItems.remove(product.id);
    repository.cartItemsList.removeWhere((element) => element.id == product.id);
    calculateDelivery();
  }
  void removeAllProducts(Product product){
    repository.cartItems.remove(product.id);
    repository.cartMap.remove(product);
    repository.cart.removeWhere((element) => element.id == product.id);
    repository.totalItems = repository.cart.length;
    calculateDelivery();
    notifyListeners();
  }

  void generateCategoryWidgets(){
    repository.categoriesList.forEach((element) {
      return repository.categoryWidgets.add(new CategoryPickerItem(value: element.name,id: element.id));
    }) ;
  }
  bool inCart(Product product){
    return repository.cartMap.keys.toList().where((element) => product.id == element.id).isNotEmpty;
  }
  bool isFavourite(Product product){
    return repository.mainPageProds.where((element) => product.id == element.id).isNotEmpty;
  }


  void getAllCities() async{
    try{
      http.Response response = await http.get(Uri.parse(baseUrl+'address/countries'));
      if(response.statusCode == 200){
        final parsed = jsonDecode(response.body)['countries'].cast<LinkedHashMap<String, dynamic>>();
        repository.allCountries = parsed.map<Country>((json) => Country.fromJson(json)).toList();
        countryNames = parsed.map<String>((json) => json['name'].toString()).toList();
        // print(repository.allCountries);
      }
    }catch(e){
      print(e);
    }
  }
  void getAboutUs() async{
        res = 'error';
    try{
      http.Response response = await http.get(Uri.parse(baseUrl+'about-us'));
      if(response.statusCode == 200){
        res = jsonDecode(response.body)['aboutUs'][0]['content'];
      }
      http.Response responseConf = await http.get(Uri.parse(baseUrl+'certificate'));
      if(responseConf.statusCode == 200){
        conf = jsonDecode(responseConf.body)['certificate'][0]['content'];
      }
      http.Response responseCert = await http.get(Uri.parse(baseUrl+'dogovor'));
      if(responseCert.statusCode == 200){
        cert = jsonDecode(responseCert.body)['dogovor']['content'];
      }
      notifyListeners();
    }catch(e){
      print(e);
    }
  }
  void calculateDelivery() async {
    var body = '';
    if(repository.isAuthenticated){
       body = jsonEncode(<String, dynamic>{
        'items': repository.cartItemsList,
        'total_sum': 0,
        'delivery_price': 0,
        'delivery_status': repository.deliveryOption?1:0,
        'pay_status': 0,
      });
    }else{
      if(repository.tempAddress==null){
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status': repository.deliveryOption?1:0,
          'pay_status': 0,
        });
      }else{
        body = jsonEncode(<String, dynamic>{
          'items': repository.cartItemsList,
          'total_sum': 0,
          'delivery_price': 0,
          'delivery_status': repository.deliveryOption?1:0,
          'pay_status': 0,
          'city': repository.tempAddress==null?'Alamty':repository.tempAddress.city,
          'country': repository.tempAddress==null?'Казахстан':repository.tempAddress.country,
        });
      }
    }
    try {
      http.Response response = await repository.session.post(
          Uri.parse(baseUrl + 'calc'), body);
      if (response.statusCode==200){
        repository.calculatedSum = jsonDecode(response.body)['total_sum'];
        repository.deliveryPrice = jsonDecode(response.body)['delivery_price'];
        notifyListeners();
      }else{
        print(jsonDecode(response.body));
      }

    } catch (e) {
      print(e);
    }
  }

  List<Product> getMainPageProductsList(){
    return repository.mainPageProds;
  }
  List<Product> getProductsList(){
    return repository.productList;
  }
}