import 'package:http/http.dart' as http;

class Session {
  Map<String, String> headers = {
    "Content-Type":"application/json",
    "Accept" : "application/json"
  };

  Future<http.Response>  get(Uri url) async {
    http.Response response = await http.get(url, headers: headers);
    return response;
  }

  Future<http.Response> post(Uri url, dynamic data) async {
    http.Response response = await http.post(url, body: data, headers: headers);
    return response;
  }
  Future<http.Response> postWithout(Uri url) async {
    http.Response response = await http.post(url, headers: headers);
    return response;
  }
  Future<http.Response> put(Uri url, dynamic data) async {
    http.Response response = await http.put(url,body: data, headers: headers);
    return response;
  }
  Future<http.Response> putWithout(Uri url,) async {
    http.Response response = await http.put(url, headers: headers);
    return response;
  }

  void addToken(String token) {
    headers['Authorization'] = 'Bearer $token';
  }
}