import 'package:sugarlife/repositories/models/address_model.dart';
class FieldsController{
  //REGISTER
  String name = '';
  String surname = '';
  String email = '';
  String password = '';
  String phone = '';
  String confPassword = '';
  String nameError = '';
  String surnameError = '';
  String emailError = '';
  String phoneError = '';
  String passwordError = '';
  String confPassError = '';

  bool err = true;
  void passwordMatch(String str, String str2){
    if (str == str2){
      confPassError =  '';
    }else {
      confPassError = 'Пароли не совпадают';
    }
  }
  bool validateRegister(){
    nameCheck(name);
    surnameCheck(surname);
    emailCheck(email);
    checkNumber(phone);
    passwordLength(password);
    passwordMatch(password, confPassword);
    if(
    passwordError ==' '
        &&nameError == ''
        &&surnameError== ''
        &&phoneError == ''
        &&confPassError == ''
        &&emailError == ''
    ){
      return false;
    }else{
      return true;
    }
  }

  void passwordLength(String str){
    if (str.length > 5) {
      passwordError = '';
    }else{
      passwordError = 'Пароль должен состоять из минимум 6 символов';
    }
  }
  void nameCheck(String str){
    if(str !=null &&str.length > 1) {
      nameError = '';
    }else{
      nameError = 'Обязательное поле';
    }
  }
  void surnameCheck(String str){
   if(str!=null && str.length > 1){
     surnameError = '';
   }
   else {
     surnameError = 'Обязательное поле';
   }
  }

  void emailCheck(String str){
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if(str == ''|| str==null){
      emailError = 'Обязательное поле';
    }
    if (!regex.hasMatch(str)) {
      emailError = 'Введите в формате example@mail.com';
    } else{
      emailError = '';
    }
  }
  void checkNumber(String str){
    String pattern = r'(^(?:7)?[0-9]{11}$)';
    RegExp regExp = new RegExp(pattern);
    // if (str.length == 0) {
    //   return 'Please enter mobile number';
    // }
    // else if (!regExp.hasMatch(str)) {
    if (str.length != 11) {
      phoneError = 'Номер телефона должен состоять из 11 цифр';
    }
    if (str == null || str == ''){
      phoneError = "Обязательное поле";
    }
    if(!regExp.hasMatch(str)) {
      phoneError = 'Введите номер в формате 77071112233';
    }
    else {
      phoneError = '';
    }
  }

  //ADDRESS

  String flatNumber = '';
  String floor = '';
  String homeNumber = '';
  String city = '';
  String street = '';

  String homeNumberErr = '';
  String cityErr = '';
  String streetErr = '';
  void homeNumberCheck(String str){
    if(str !=null &&str.length >= 1) {
      nameError = '';
    }else{
      nameError = 'Обязательное поле';
    }
  }
  void cityCheck(String str){
    if(str !=null &&str.length >= 1) {
      nameError = '';
    }else{
      nameError = 'Обязательное поле';
    }
  }
  void streetCheck(String str){
    if(str !=null &&str.length >= 1) {
      nameError = '';
    }else{
      nameError = 'Обязательное поле';
    }
  }
  void setAddress(AddressModel address){
     if(address!=null){
       flatNumber = address.flatNumber;
       floor = address.floor;
       homeNumber = address.homeNumber;
       city = address.city;
       street = address.street;
     }
  }
  bool validateAddress(){
    homeNumberCheck(homeNumber);
    cityCheck(city);
    streetCheck(street);
    if(
    homeNumberErr ==' '
        &&cityErr == ''
        &&streetErr== ''
    ){
      return false;
    }else{
      return true;
    }
  }
  bool validateAuth(){
    passwordLength(password);
    emailCheck(email);
    if(
    emailError ==' '
        &&passwordError == ''
    ){
      return false;
    }else{
      return true;
    }
  }

}