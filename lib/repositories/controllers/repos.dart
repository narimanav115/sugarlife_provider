import 'dart:collection';
import 'package:category_picker/category_picker_item.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sugarlife/repositories/controllers/session.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/repositories/models/article_model.dart';
import 'package:sugarlife/repositories/models/card_model.dart';
import 'package:sugarlife/repositories/models/category_model.dart';
import 'package:sugarlife/repositories/models/city_model.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/repositories/models/slide_model.dart';
import 'package:sugarlife/repositories/models/stocks_model.dart';
import 'package:sugarlife/repositories/models/user_model.dart';

class Repository{
  String pickupTime = '12:00-13:00';
  String pickupDate = '';
  String authError = '';
  dynamic calculatedSum = 1;
  dynamic deliveryPrice =1;
  List<Product> tempProds = [];
  List<Product> productList = [];
  List<Product> mainPageProds = [];
  List<Product> allProds = [];
  List<CategoryModel> categoriesList = [];
  List<SlideModel> slides = [];
  List<Article> articles = [];
  Map <int, int> cartItems = {};
  List<Product> favorites = [];
  List<AddressModel> addresses = [];
  List<OrderModel> orders = [];
  List<Stock> stocks = [];
  List<CompanyContact> companyContacts = [];
  List<CompanyAddress> companyAddresses = [];
  String token = '';
  List<CategoryPickerItem> categoryWidgets = [
    CategoryPickerItem(
      value: "Все товары",
      id: -1,
    ),
  ];
  Map<String, String> headers = {
    "Content-Type":"application/json",
    "Accept" : "application/json"
  };

  List<Product> cart = [];
  Map<Product, int> cartMap = Map();
  double totalSum = 0;
  int totalItems = 0;
  Product selectedProd = Product();
  bool selectedIsUploaded = false;
  int totalPrice = 0;
  LinkedHashMap<String,dynamic> mapp = LinkedHashMap();

  bool isAuthenticated = false;
  UserModel user;
  UserModel tempUser = UserModel(name: 'Имя',  surname: 'Фамилия',email: 'Email', phone: 'Номер телефона');
  UserModel tokenUser = UserModel(id: 2, name: 'Test2', password: '123456', surname: 'Test2',email: 'test2@test.com');
  UserModel socialUser;
  AddressModel tempAddress;
  List<CartItemModel> cartItemsList = [];
  OrderModel finalOrder;
  Session session = Session();
  void makeFinalOrder() {
    finalOrder = OrderModel(price: totalSum, deliveryPrice: 1000, cartItemModel: cartItemsList);
    finalOrder.toString();
  }

  List<CardModel> cards = [];

  List<Country> allCountries = [];
  bool deliveryOption = false;
  CardModel selectedCard;

  CameraPosition cityPos = CameraPosition(
      target: LatLng(
        43.0,
        76.0,
      )
  );

  Set<Marker> markers= Set();
  String getAddressesOfCompany(){
    String temp = '';
    companyAddresses.forEach((element) {temp += '${element.address}\n';});
    return temp;
  }
  String getContactsOfCompany(){
    String temp = '';
    companyContacts.forEach((element) {temp += '${element.phoneNumber}\n';});
    return temp;
  }

}