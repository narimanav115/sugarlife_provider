// Place fonts/SugarLife.ttf in your fonts/ directory and
// add the following to your pubspec.yaml
// flutter:
//   fonts:
//    - family: SugarLife
//      fonts:
//       - asset: fonts/SugarLife.ttf
import 'package:flutter/widgets.dart';

class SugarIcons {
  SugarIcons._();

  static const String _fontFamily = 'SugarLife';

  static const IconData assignment = IconData(0xe904, fontFamily: _fontFamily);
  static const IconData back = IconData(0xe908, fontFamily: _fontFamily);
  static const IconData doublecard = IconData(0xe90a, fontFamily: _fontFamily);
  static const IconData edit = IconData(0xe90b, fontFamily: _fontFamily);
  static const IconData filter = IconData(0xe90d, fontFamily: _fontFamily);
  static const IconData info_circle = IconData(0xe90e, fontFamily: _fontFamily);
  static const IconData instagramm = IconData(0xe90f, fontFamily: _fontFamily);
  static const IconData Location = IconData(0xe910, fontFamily: _fontFamily);
  static const IconData menu = IconData(0xe912, fontFamily: _fontFamily);
  static const IconData obscure_false = IconData(0xe913, fontFamily: _fontFamily);
  static const IconData obscure_true = IconData(0xe914, fontFamily: _fontFamily);
  static const IconData Phone = IconData(0xe917, fontFamily: _fontFamily);
  static const IconData pie = IconData(0xe918, fontFamily: _fontFamily);
  static const IconData profile = IconData(0xe905, fontFamily: 'icomoon');
  static const IconData categories = IconData(0xe906, fontFamily: 'icomoon');
  // static const IconData shop_cart = IconData(0xe91c, fontFamily: _fontFamily);
  static const IconData time = IconData(0xe91d, fontFamily: _fontFamily);
  static const IconData home = IconData(0xe900, fontFamily: _fontFamily);
  static const IconData email = IconData(0xe902, fontFamily: _fontFamily);
  static const IconData email_rounded = IconData(0xe903, fontFamily: _fontFamily);
  static const IconData user = IconData(0xe905, fontFamily: _fontFamily);
  static const IconData lotus = IconData(0xe906, fontFamily: _fontFamily);
  static const IconData shop_cart = IconData(0xe907, fontFamily: _fontFamily);

}
