import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/fields_controller.dart';
import 'package:sugarlife/repositories/controllers/repos.dart';
import 'package:sugarlife/repositories/controllers/session.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/services/firebase_cloud_messaging.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import '';
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);

    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
      AndroidServiceWorkerController.instance();

      serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async {
          print(request);
          return null;
        },
      );
    }
  }

  registerOnFirebase();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final Repository repo = Repository();
  final FlutterSecureStorage secureStorage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserController>(create: (_) => UserController(repository: repo, storage: secureStorage)),
        ChangeNotifierProvider<ShoppingController>(create: (_) => ShoppingController(repository: repo, storage: secureStorage)),
        // ChangeNotifierProvider<FieldsController>(create: (_) => FieldsController()),
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.white,
          fontFamily: 'Lato',
          backgroundColor: Color(0xffF7F7F7),
          canvasColor: Colors.white,
          // accentColor: Colors.white,
          scaffoldBackgroundColor: Color(0xffF7F7F7),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        title: 'Sugar Life',
        home: HomePage(),
        routes: {
          '/home': (context)=>HomePage(),
        },
      ),
    );
  }
}

