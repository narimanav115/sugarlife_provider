import 'dart:io';

import 'package:sugarlife/main.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();

  Future initialize() async {
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
        onMessage: onMessage,
        onLaunch: onLaunch,
        onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
        onResume: onResume);
  }
}

Future<dynamic> onMessage(Map<String, dynamic> message) async {
  Future<void> showNotification(
      int id,
      String notificationTitle,
      String notificationContent,
      String payload, {
        String channelId = '1234',
        String channelTitle = 'Android Channel',
        String channelDescription = 'Default Android Channel for notifications',
        Priority notificationPriority = Priority.high,
        Importance notificationImportance = Importance.max,
      }) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        channelId, channelTitle, channelDescription,
        playSound: false,
        importance: notificationImportance,
        priority: notificationPriority,
        styleInformation: BigTextStyleInformation(''),
        color: Color(0xffFE00AE));
    var iOSPlatformChannelSpecifics =
    new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      id,
      notificationTitle,
      notificationContent,
      platformChannelSpecifics,
      payload: payload,
    );
  }

  print(message);
  // showNotification(1235, message['aps']['alert']['title'], message['aps']['alert']['body'],
  //     message['notification'].toString());
  showNotification(1235, message['data']['title'], message['data']['body'],
      message['data'].toString());
}

Future<dynamic> onResume(Map<String, dynamic> message) async {
  print('onResume $message');
}

Future<dynamic> onLaunch(Map<String, dynamic> message) async {
  print('onLaunch $message');
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  var initializationSettingsAndroid =
  new AndroidInitializationSettings('@mipmap/ic_launcher');
  var initializationSettingsIOS = new IOSInitializationSettings();
  var initializationSettings = new InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: onSelectNotification);
  Future<void> showNotification(
      int id,
      String notificationTitle,
      String notificationContent,
      String payload, {
        String channelId = '1234',
        String channelTitle = 'Android Channel',
        String channelDescription = 'Default Android Channel for notifications',
        Priority notificationPriority = Priority.high,
        Importance notificationImportance = Importance.max,
      }) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        channelId, channelTitle, channelDescription,
        playSound: false,
        importance: notificationImportance,
        priority: notificationPriority,
        color: Color(0xffff0000));
    var iOSPlatformChannelSpecifics =
    new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      id,
      notificationTitle,
      notificationContent,
      platformChannelSpecifics,
      payload: payload,
    );
  }

  print("coming");
  print("background message");
  showNotification(1235, message['data']['title'], message['data']['body'],
      message['data'].toString());
  // showNotification(1235, message['aps']['alert']['title'], message['aps']['alert']['body'],
  //     message['notification'].toString());
}

registerOnFirebase() async {

  flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  await Firebase.initializeApp();
  var initializationSettingsAndroid =
  new AndroidInitializationSettings('@mipmap/ic_launcher');
  var initializationSettingsIOS = new IOSInitializationSettings();
  var initializationSettings = new InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: onSelectNotification);
  await PushNotificationService().initialize().whenComplete(
          () async => await FirebaseMessaging().subscribeToTopic('android'));
}

Future<dynamic> onSelectNotification(String payload) async {
  print('on select');
}

