import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class SugarLifeTheme {
  static TextStyle bigStyle = TextStyle(
      fontFamily: 'Bebas Neue', fontSize: 30, fontWeight: FontWeight.w700);
  static TextStyle regular = TextStyle(
    fontFamily: 'Lato',
    fontSize: 15,
    fontWeight: FontWeight.w300,
  );
  static TextStyle regularBold = TextStyle(
    fontFamily: 'Lato',
    fontSize: 13,
    fontWeight: FontWeight.w600,
  );
  static TextStyle whiteText = TextStyle(
      fontFamily: 'Avanti',
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: Colors.white);
  static TextStyle buttonStyle = TextStyle(
      fontFamily: 'Avanti', fontSize: 14, fontWeight: FontWeight.w400);
  static const Color grey = Color(0xFFE8F5E2);
  static Color yellow = Color(0xFFFFE24B);
  static const Color shadowGrey = Color(0xffE5E5E5);
  static Color green = Color(0xFF59B439);
}
