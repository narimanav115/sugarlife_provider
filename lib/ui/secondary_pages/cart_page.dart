import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/secondary_pages/final_order_page.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/cart_item.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isAuthenticated = Provider.of<UserController>(context).repository.isAuthenticated;
    return Scaffold(
      extendBody: true,
      appBar: SugarAppbar(isCartPage: true),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              RouteNameBar(label: 'Корзина'),

              Provider.of<ShoppingController>(context, listen: false).repository.cartItems.isNotEmpty
                  ?
              ListView.builder(
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                    },
                    child: CartItem(
                        product: Provider.of<ShoppingController>(context, listen: false).repository.cartMap.entries.toList()[i].key,
                        number: Provider.of<ShoppingController>(context, listen: false).repository.cartMap.entries.toList()[i].value,
                    ),
                  );
                },
                itemCount: Provider.of<ShoppingController>(context).repository.cartMap.entries.length,
                physics: NeverScrollableScrollPhysics(),
              )
                  :
                  Text('Ваша корзина пуста'),
            ],
          ),
        ),
      ),
      bottomNavigationBar:
      Provider.of<ShoppingController>(context, listen: false).repository.cartMap.isNotEmpty
          ?
      Container(
        color: Colors.transparent,
        margin: EdgeInsets.fromLTRB(20, 0, 20, 30),
        child: ActionButton(
          padding: EdgeInsets.zero,
          label: 'Оформить заказ',
          onPressed: () async{
            // if(!isAuthenticated){
            //   showAuthDialog(context, '', 'Чтобы оформить заказ пожалуйста авторизуйтесь');
            // }else{
              if(await Provider.of<UserController>(context, listen: false).calculateDelivery()){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>FinalOrderPage()));
              }else{
                Navigator.push(context, MaterialPageRoute(builder: (context)=>FinalOrderPage()));
              }
            // }
          },
          backgroundColor: Colors.transparent,),
      )
          :SizedBox()
      ,
    );
  }
}
