import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/card_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/secondary_pages/new_card_page.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/buttons/outlined_green_button.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/cart_item.dart';

class PaymentOption extends StatefulWidget {
  @override
  _PaymentOptionState createState() => _PaymentOptionState();
}

class _PaymentOptionState extends State<PaymentOption> {
  @override
  Widget build(BuildContext context) {
    List<CardModel> cards = Provider.of<UserController>(context).repository.cards;
    return Scaffold(
      appBar: SugarAppbar(isCartPage: true),
      body: SingleChildScrollView(
        child: Column(
          children: [
            RouteNameBar(label: 'Способ оплаты'),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, i) {
                return ListTile(
                  title: Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 5, 10),
                      decoration: OrderItem.decoration,
                      child: ListTile(
                        leading: Image.asset('assets/circles.png'),
                        title: Text(cards[i].cardFirstSix+'******'+cards[i].cardLastFour) ,
                        trailing: GestureDetector(
                          child: Icon(Icons.delete, color: Colors.red,),
                          onTap: () async {
                            if(await Provider.of<UserController>(context,listen: false).deleteCard(cards[i].id)){
                              showAlertDialog(context, '', 'Карта успешно удалена');
                            }else{
                              showAlertDialog(context, 'Что-то пошло не так', 'Возможны работы на сервере');
                            }
                          },
                        ),
                      )
                  ),

                );
              },
              itemCount: Provider.of<UserController>(context).repository.cards.length,
            ),
            OutlinedActionButton(label: 'Добавить карту', onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>AddNewCard()));

            }),
            // ActionButton(label: 'Подтвердить', onPressed: (){})

          ],
        ),
      ),

    );
  }
}
