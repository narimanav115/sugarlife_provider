import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/secondary_pages/final_order_page.dart';
import 'package:sugarlife/ui/secondary_pages/profile/address_add.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class DeliveryOption extends StatelessWidget {
  final AddressModel address;
  DeliveryOption({this.address});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SugarAppbar(isCartPage: true),
      body: SingleChildScrollView(
        child: Column(
          children: [
            RouteNameBar(label: 'Способ доставки'),
            DeliveryOptionsBody(address: address,),
            ActionButton(label: 'Подтвердить', onPressed: (){
              Provider.of<UserController>(context, listen: false).calculateDelivery();
              Navigator.push(context, MaterialPageRoute(builder: (context)=>FinalOrderPage()));
            })
          ],
        ),
      ),
    );
  }
}

List<String> times = [
  '9:00 - 10:00',
  '10:00 - 11:00',
  '11:00 - 12:00',
  '12:00 - 13:00',
  '13:00 - 14:00',
  '15:00 - 16:00',
  '17:00 - 18:00',
  '18:00 - 19:00',
];
Future<List<String>> getTime(String filter) async {
  return await times.where((String item)=>item.startsWith(filter)).toList();
}
class DeliveryOptionsBody extends StatefulWidget {
  final AddressModel address;
  DeliveryOptionsBody({this.address});
  @override
  _DeliveryOptionsBodyState createState() => _DeliveryOptionsBodyState();
}

class _DeliveryOptionsBodyState extends State<DeliveryOptionsBody> {
  DeliveryType selected = DeliveryType.pickup;
  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year, DateTime.now().month,DateTime.now().day),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month+2,)
    );
    picked = picked.toLocal();
    
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        Provider.of<UserController>(context,listen: false).repository.pickupDate = "${selectedDate.toLocal()}".split(' ')[0];
      });
  }
  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
          title: const Text('Самовывоз'),
          leading: Radio(
            value: DeliveryType.pickup,
            groupValue: selected,
            onChanged: (DeliveryType value) {
              setState(() {
                Provider.of<UserController>(context,listen: false).calculateDelivery();
                selected = value;
                Provider.of<UserController>(context,listen: false).repository.deliveryOption = false;
              });
            },
          ),
        ),
        if(selected==DeliveryType.pickup)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 36),
              //   child: Text(widget.address.toString(),textAlign: TextAlign.start, style: TextStyle(color: Colors.grey),),
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 36),
                child: Text(''),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25),
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  showSelectedItem: true,
                  showSearchBox: true,
                  searchBoxDecoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                    labelText: "Выберите время",
                  ),
                  selectedItem: Provider.of<UserController>(context).repository.pickupTime,
                  onFind: (String filter) => getTime(filter),
                  label: "Время самовывоза",
                  popupItemDisabled: (String s) => s.startsWith('I'),
                  onChanged: (e){
                    setState(() {
                      Provider.of<UserController>(context,listen: false).repository.pickupTime = e;
                    });
                  },
                ),
              ),
              Stack(
                children:[
                  CustomTextField(
                        enabled: false,
                        hintText: "${selectedDate.toLocal()}".split(' ')[0],
                        onChanged: (e){
                        },
                    ),
                  Positioned(
                    right: 30,
                    bottom: 5,
                    child: IconButton(
                      onPressed: () => _selectDate(context),
                      icon: Icon(Icons.date_range_outlined,color: SugarLifeTheme.green,),
                    ),
                  )
                ],
              ),
            ],
          ),

        ListTile(
          title: const Text('Доставка'),
          subtitle: widget.address!=null?
          Text(widget.address!=null?widget.address.toString():'',
            textAlign: TextAlign.start,
            style: TextStyle(color: Colors.grey),
          ):Visibility(
              visible: widget.address==null,
              child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>AddressPage(label: 'Добавьте адрес')));
                  },
                  child: Text('Добавить адрес', style: TextStyle(decoration: TextDecoration.underline, color: SugarLifeTheme.green),),
              )
          ),
          leading: Radio(
            value: DeliveryType.delivery,
            groupValue: selected,
            onChanged: (DeliveryType value) {
              setState(() {
                selected = value;
                Provider.of<UserController>(context,listen: false).calculateDelivery();
                Provider.of<UserController>(context,listen: false).repository.deliveryOption = true;
              });
            },
          ),
        ),

      ],
    );
  }
}

class ShippingCompanies extends StatefulWidget {
  @override
  _ShippingCompaniesState createState() => _ShippingCompaniesState();
}

class _ShippingCompaniesState extends State<ShippingCompanies> {

  ShippingCompany selected = ShippingCompany.ponyExpress;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 36),
          child: Text('Службы доставки'),
        ),
        ListTile(
          title: Row(
            children: [
              Image.asset('assets/pony.png'),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: const Text('Pony Express'),
              ),
            ],
          ),
          leading: Radio(
            value: ShippingCompany.ponyExpress,
            groupValue: selected,
            onChanged: (ShippingCompany value) {
              setState(() {
                selected = value;
              });
            },
          ),
        ),
        ListTile(
          title: Row(
            children: [
              Image.asset('assets/air.png'),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: const Text('Air Astana'),
              ),
            ],
          ),
          leading: Radio(
            value: ShippingCompany.airAstana,
            groupValue: selected,
            onChanged: (ShippingCompany value) {
              setState(() {
                selected = value;
              });
            },
          ),
        ),
        ListTile(
          title: Row(
            children: [
              Image.asset('assets/post.png'),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: const Text('Post Express'),
              ),
            ],
          ),
          leading: Radio(
            value: ShippingCompany.postExpresss,
            groupValue: selected,
            onChanged: (ShippingCompany value) {
              setState(() {
                selected = value;
              });
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 16),
          child: Text('Стоимость зависит от веса посылки и будет указана в накладной. Ознакомиться с ценами вы можете на сайте компании'),
        ),
      ],
    );
  }
}

enum DeliveryType { delivery, pickup}
enum ShippingCompany { ponyExpress, airAstana, postExpresss}