import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
class ArticlesWebView extends StatelessWidget {
  final data;
  ArticlesWebView({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: SugarAppbar(),
        body: SafeArea(

          child: InAppWebView(
          initialData: InAppWebViewInitialData(
              data: data,
          ),
              initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                    supportZoom: true,
                    minimumFontSize: 20
              ),
            )
        ),
      )
    );
  }
}

