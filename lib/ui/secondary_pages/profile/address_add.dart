import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/repositories/models/city_model.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/final_order_page.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class AddressPage extends StatelessWidget {
  final label;
  final AddressModel address;
  AddressPage({this.label, this.address});
  @override
  Widget build(BuildContext context) {
    String flatNumber= '';
    String floor = '';
    String homeNumber = '';
    String city = '';
    String country = '';
    String street = '';
    List<String> cities = [];
    List<Country> countries = Provider.of<ShoppingController>(context).repository.allCountries;
    List<String> allCountries = Provider.of<ShoppingController>(context).countryNames;
    bool isAuthenticated = Provider.of<ShoppingController>(context).repository.isAuthenticated;
    getCountry(String filter) async{

      return allCountries.where((String item)=>item.startsWith(filter)).toList();
    }
    getCity(String filter) async{
      return cities.where((String item)=>item.startsWith(filter)).toList();
    }
    return Scaffold(
        appBar: SugarAppbar(isCartPage: true),
        body: SingleChildScrollView(
          child: Column(
            children: [
              RouteNameBar(label: label),
              Container(
                height: 50,
                margin: EdgeInsets.only(top:25,left: 26,right: 26),
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  showSelectedItem: true,
                  showSearchBox: true,
                  searchBoxDecoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                    labelText: "Страна",
                  ),
                  selectedItem: address==null?'':address.country,
                  onFind: (String filter) => getCountry(filter),
                  label: "Страна",
                  popupItemDisabled: (String s) => s.startsWith('I'),
                  onChanged: (e){
                    country = e;
                    cities = countries.firstWhere((element) => element.name == country).cities.map<String>((element) => element.name.toString()).toList();
                    print(cities);
                    },
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(top:25,left: 26,right: 26),
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  showSelectedItem: true,
                  showSearchBox: true,
                  searchBoxDecoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                    labelText: "Найти город",
                  ),
                  selectedItem: address==null?'':address.city,
                  onFind: (String filter) => getCity(filter),
                  label: "Город",
                  popupItemDisabled: (String s) => s.startsWith('I'),
                  onChanged: (e){
                    city = e;
                  },
                ),
              ),
              // new DropdownButtonFormField<String>(
              //   items: Provider.of<ShoppingController>(context).cityNames.map((String value) {
              //     return new DropdownMenuItem<String>(
              //       value: value,
              //       child: new Text(value),
              //     );
              //   }).toList(),
              //   onChanged: (_) {},
              // ),
              CustomTextField(
                initialValue:address==null?'':address.flatNumber,
                hintText: address==null?'Номер квартиры':address.flatNumber,
                onChanged: (e){
                  flatNumber = e;
                },
                headText: '',),
              CustomTextField(
                initialValue: address==null?'':address.floor,
                hintText: address==null?'Этаж':address.floor,
                onChanged: (e){
                  floor = e;
                },
                headText: '',),
              CustomTextField(
                initialValue: address!=null?address.homeNumber:'',
                hintText: address==null?'Номер дома/частного дома':address.homeNumber,
                onChanged: (e){
                  homeNumber = e;
                },
                headText: '',),
              CustomTextField(
                initialValue: address==null?'':address.street,
                hintText: address==null?'Улица':address.street,
                onChanged: (e){
                  street = e;
                },

                headText: '',),
              // CustomTextField(
              //   initialValue: address==null?'':address.city,
              //   hintText: address==null?'Город':address.city,
              //   onChanged: (e){
              //     city = e;
              //   },
              //   headText: '',
              // ),

              Visibility(
                  visible: address!=null,
                  child: ActionButton(
                    backgroundColor: Colors.transparent,
                    label: 'Изменить',
                    onPressed: ()async{
                      if(address!=null) {
                        var check = await Provider.of<UserController>(context,listen: false).updateAddress(
                          id: address.id,
                          homeNumber: homeNumber==''?address.homeNumber:homeNumber,
                          flatNumber: flatNumber==''?address.flatNumber:flatNumber,
                          street: street==''?address.street:street,
                          city: city==''?address.city:city,
                          floor: floor==''?address.floor:floor,
                          country: country==''?address.country:country
                        );
                        if(check){
                          Navigator.of(context).push(MaterialPageRoute(builder: (_)=>HomePage()));
                          showAlertDialog(context, '', 'Данные успешно обновлены');
                        }else{
                          showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте правильность введенных данных');
                        }
                      }
                    },
                  )
              ),
              Visibility(
                  visible: address!=null,
                  child: ActionButton(
                    backgroundColor: Colors.transparent,
                    color: Colors.red,
                    label:'Удалить',
                    onPressed: () async{
                      if(address!=null){
                        if(await Provider.of<UserController>(context,listen: false).deleteAddress(id: address.id)){
                          Navigator.push(context, MaterialPageRoute(builder: (_)=>HomePage()));
                          showAlertDialog(context, '', 'Успешно удалено');
                        }else{
                          showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте попробуйте позже');
                        }
                      }

                    },
                  )
              ),
              Visibility(
                visible: address == null,
                child: ActionButton(
                    backgroundColor: Colors.transparent,
                    label: 'Добавить',
                    onPressed: () async {
                    if(isAuthenticated){
                      if(address==null){
                        var check = await Provider.of<UserController>(context,listen: false).addAddress(
                          homeNumber: homeNumber,
                          flatNumber: flatNumber==''? 0:flatNumber,
                          street: street,
                          city: city,
                          floor: floor==''? 0:floor,
                          country: country,
                        );
                        if(check){
                          Navigator.of(context).push(MaterialPageRoute(builder: (_)=>HomePage()));
                          showAlertDialog(context, '', 'Адрес успешно добавлен');
                        }else{
                          showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте правильность введенных данных');
                        }
                      }
                    }else{
                      Provider.of<UserController>(context,listen: false).repository.tempAddress = AddressModel(homeNumber:homeNumber,flatNumber:flatNumber,city: city,street: street,floor: floor,country: country);
                      Provider.of<UserController>(context,listen: false).calculateDelivery();
                      Navigator.of(context).pop(context);
                      showAlertDialog(context, '', 'Адрес успешно добавлен');
                      Navigator.of(context).pop(context);
                    }
                }),
              ),
            ],
          ),
        )
    );
  }
}
