import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class ChangePassword extends StatefulWidget {
  var oldPassObs = true;
  var newPassObs = true;
  var confPassObs = true;
  String oldPass;
  String newPass;
  String confirmNewPass;
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
          child: Column(children: [
            RouteNameBar(label: 'Сменить пароль'),
            CustomTextField(
              hintText: '**********',
              onChanged: (e){widget.oldPass = e;},
              headText: 'Текущий пароль',
              obscure: widget.oldPassObs,
              icon: Icons.margin,
              onTapIcon: (){
                setState(() {
                  widget.oldPassObs = !widget.oldPassObs;
                });
              },
            ),
            CustomTextField(
                hintText: '**********',
                onChanged: (e){widget.newPass = e;},
                headText: 'Новый пароль',
                obscure: widget.newPassObs,
                icon: Icons.margin,
                onTapIcon: (){
                  setState(() {
                    widget.newPassObs = !widget.newPassObs;
                  });
                },
            ),
            CustomTextField(
                hintText: '**********',
                onChanged: (e){
                  widget.confirmNewPass = e;
                },
                headText: 'Подтверждение нового пароля',
                obscure: widget.confPassObs,
                icon: Icons.margin,
                onTapIcon: (){
                  setState(() {
                    widget.confPassObs = !widget.confPassObs;
                  });
                },
            ),
            ActionButton(duration: 1000, label: 'Сменить пароль',
              onPressed: () async {
                var result = await Provider.of<UserController>(context,listen: false).changePassword(oldPassword: widget.oldPass, newPassword: widget.newPass,conNewPassword: widget.confirmNewPass);
                if(result['status']){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage()));
                  showAlertDialog(context, '', 'Пароль успешно сменен ');
                }else{
                  showAlertDialog(context, 'Произошла ошибка', '${result['error']}');
                }
              }
            ),
          ],

          )),
    );
  }
}
