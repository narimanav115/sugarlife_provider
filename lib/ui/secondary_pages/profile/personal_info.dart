import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/user_model.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class PersonalInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserModel user;
    Provider.of<UserController>(context, listen: false).repository.isAuthenticated
        ? user = Provider.of<UserController>(context).repository.user
        : user = null;
    String name = '';
    String surname = '';
    String email = '';
    String phone = '';

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            RouteNameBar(label: 'Персональная информация'),
            CustomTextField(
              hintText: '${user.name}',
              keyBoard: TextInputType.name,
              onChanged: (e){
                name = e;
                name ==''
                    ? name = user.name
                    : name = e;
              },
            ),
            CustomTextField(
                hintText: '${user.surname}',
                keyBoard: TextInputType.name,
                onChanged: (e){
                  surname = e;
                  surname==''
                      ? surname = user.surname
                      : surname = e;
                },
            ),
            CustomTextField(
                hintText: '${user.email}',
                keyBoard: TextInputType.emailAddress,
                onChanged: (e){
                  email = e;
                  email ==''
                      ? email = user.email
                      : email = e;
                },
            ),
            CustomTextField(
                hintText: '${user.phone}',
                keyBoard: TextInputType.number,
                onChanged:(e){
                  phone = e;
                  phone == ''
                      ? phone = user.phone
                      : phone = e;
                },
            ),
            ActionButton(label: 'Сохранить', onPressed: ()async{
              var check = await Provider.of<UserController>(context, listen: false).changeUserData(
                  name: name==''?user.name:name,
                  surname: surname==''?user.surname:surname,
                  email: email==''?user.email:email,
                  phone: phone==''?user.phone:phone
              );
              if(check){
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage()));
                showAlertDialog(context, '', 'Данные успешно обновлены');
              }else{
                showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте правильность введенных данных');
              }

            })
          ],
        ),
      ),
    );
  }
}
