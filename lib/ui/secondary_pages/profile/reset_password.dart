import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/fields_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

// ignore: must_be_immutable
class ResetPassword extends StatefulWidget {
  bool obscure = true;
  FieldsController controller = FieldsController();
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: SugarAppbar(isCartPage: true),
        body: SingleChildScrollView(
          child: Column(
            children: [
              RouteNameBar(label: 'Сброс пароля'),
              Container(
                  padding: EdgeInsets.all(20),
                  child: Center(child: Text('Введите email, если он зарегистрирован, вам будет отправлен новый пароль',))),
              CustomTextField(
                hintText: 'example@example.example',
                onChanged: (e){
                  widget.controller.email = e;
                  widget.controller.emailCheck(widget.controller.email);
                },
                headText: widget.controller.emailError,),
              ActionButton(duration: 1500, label: 'Отправить', onPressed: () async {
                if(widget.controller.email!=''){
                  var check = await Provider.of<UserController>(context,listen: false).resetPassword(widget.controller.email);
                  if(check){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage()));
                    showAlertDialog(context, '', 'Новый пароль был отправлен на вашу почту');
                  }else{
                    showAlertDialog(context, 'Ошибка', 'Попробуйте позднее');
                  }
                }
              }),
            ],
          ),
        )
    );
  }
}

