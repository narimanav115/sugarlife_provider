import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/cart_item.dart';


class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  Timer timer;
  List<OrderModel> orders = [];
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    setState(() {

    });
    _refreshController.refreshCompleted();
  }
  @override
  Widget build(BuildContext context) {
    orders = Provider.of<UserController>(context).ordersHistory;
    Provider.of<UserController>(context,listen: false).getOrdersHistory();


    return Scaffold(
      appBar: SugarAppbar(),
      body:
          Flex(
            direction: Axis.vertical,
            children: [
              Flexible(
                child: SmartRefresher(
                  onRefresh: _onRefresh,
                  controller: _refreshController,
                  child: orders.isNotEmpty? ListView.builder(
                    shrinkWrap: false,
                    reverse: true,
                    itemBuilder: (context, i) {
                      return HistoryTile(
                        id: orders[i].id,
                        totalSum: orders[i].totalPrice,
                        totalItems: orders[i].positions,
                        status: orders[i].status,
                        date: orders[i].creationDate,
                      );
                    },
                    itemCount: orders.length,
                  ) : Container(margin: EdgeInsets.all(20),child: Center(child: Text('У вас пока нет заказов'))),
                ),

              ),
            ],
          )
    );
  }
}

// ignore: must_be_immutable
class HistoryTile extends StatefulWidget {
  int id;
  var totalItems;
  var totalSum;
  var status;
  var date;
  HistoryTile({
    this.date,
    this.id,
    this.totalSum,
    this.status= 'В обработке',
    this.totalItems
  });

  @override
  _HistoryTileState createState() => _HistoryTileState();
}

class _HistoryTileState extends State<HistoryTile> {
  bool clicked = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: (){
            setState(() {
              clicked=!clicked;
            });
            Provider.of<UserController>(context,listen: false).getOrder(widget.id);
          },
          child: Padding(
            padding: EdgeInsets.fromLTRB(32, 16, 32,0),

            child: DecoratedBox(
              decoration: BoxDecoration(
                color: !clicked?Colors.white:Color(0xffE8F5E2),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 4,
                    spreadRadius: 3,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Row(children: [
                      Text('Заказ №${widget.id}')
                    ],),
                    SizedBox(height: 16,),
                    Row(children: [
                      Text('${widget.totalItems} позиции '),
                      Text('${widget.totalSum} ₸',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),),
                    ],),
                    SizedBox(height: 16,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('${widget.date}'.substring(0, widget.date.indexOf('T'))),
                        Text('${widget.status}',style: TextStyle(color: SugarLifeTheme.green),)
                      ],
                    ),
                  ],

                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: clicked,
          child: Provider.of<UserController>(context).allClickedOrders[widget.id]!=null
              ?
          Container(
            padding: EdgeInsets.only(left: 40,right: 30),
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, i) {
                return OrderItem(
                  measureType: Provider.of<UserController>(context).allClickedOrders[widget.id][i].measureType,
                  weight: Provider.of<UserController>(context).allClickedOrders[widget.id][i].weight,
                  productImage: Provider.of<UserController>(context).allClickedOrders[widget.id][i].imageUrl,
                  quantity: Provider.of<UserController>(context).allClickedOrders[widget.id][i].quantity,
                  product: Provider.of<UserController>(context).allClickedOrders[widget.id][i].itemName,
                  price: Provider.of<UserController>(context).allClickedOrders[widget.id][i].price,
                );
              },
              itemCount: Provider.of<UserController>(context).allClickedOrders[widget.id].length,
              physics: NeverScrollableScrollPhysics(),
            ),
          )
              :
          Container(
            margin: EdgeInsets.only(top: 20),
              child: LoadingCircle(color: SugarLifeTheme.green,)
          ),
        ),
      ]
    );
  }
}
