import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/product_container.dart';
import 'package:sugarlife/ui/widgets/scaffold_body.dart';

class Favourites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return ScaffoldConstructor(children: [
      RouteNameBar(label: 'Избранное'),
      Flexible(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 20),
          child: GridView.builder(
            shrinkWrap: false,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 8,
                mainAxisSpacing: 24,
                childAspectRatio: (w / 2 - 20) / 285,
                crossAxisCount: 2),
            itemBuilder: (context, i) {
              return ProductContainer(isMainPage:false, product: Provider.of<UserController>(context).repository.favorites[i],);
            },
            itemCount: Provider.of<UserController>(context).repository.favorites.length,
          ),
        ),
      ),
    ]);
  }
}
