import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:webview_flutter/webview_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/fields_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/profile/registration.dart';
import 'package:sugarlife/ui/secondary_pages/profile/reset_password.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class Authorization extends StatefulWidget {
  bool obscure = true;
  FieldsController controller = FieldsController();
  @override
  _AuthorizationState createState() => _AuthorizationState();
}

class _AuthorizationState extends State<Authorization> {

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    bool err = false;
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: SugarAppbar(isCartPage: true),
        body: SingleChildScrollView(
          child: Column(
            children: [
              RouteNameBar(label: 'Авторизация'),
              CustomTextField(
                keyBoard: TextInputType.emailAddress,
                hintText: 'E-mail',
                onChanged: (e){
                  widget.controller.email = e;
                    widget.controller.emailCheck(widget.controller.email);
                  },
                headText: widget.controller.emailError,),
              CustomTextField(
                keyBoard: TextInputType.visiblePassword,
                icon: Icons.margin,
                obscure: widget.obscure,
                hintText: 'Пароль',
                onTapIcon: (){
                  setState(() {
                    widget.obscure = !widget.obscure;
                  });
                },
                onChanged: (e){
                widget.controller.password = e;
                   setState(() {
                     err = widget.controller.validateAuth();
                   });
                },
                headText: widget.controller.passwordError,
              ),
              ActionButton(duration: 1000, label: 'Войти', onPressed: () async {

                  if(widget.controller.password!=''&&widget.controller.email!=''){
                     var check = await Provider.of<UserController>(context,listen: false).authenticate(widget.controller.email, widget.controller.password);
                     if(check){
                       Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage()));
                       showAlertDialog(context, '', 'Успешно авторизованы');
                     }else{
                       showAlertDialog(context, 'Ошибка авторизации', 'Неверный логин или пароль');
                     }
                  }
              }),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                    child: Text('Забыли пароль?'),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ResetPassword()));
                    },
                ),
              ),
              //TODO SOCIAL AUTH
              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: Text('Войти с помощью соцсетей'),
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //   Container(
              //     width: MediaQuery.of(context).size.width/2.5,
              //       child: SocialLoginButton(
              //           label: 'Facebook',
              //           onPressed: (){
              //
              //           },
              //           icon: 'assets/facebook.png')),
              //   SizedBox(width: 16  ,),
              //   Container(
              //     width: MediaQuery.of(context).size.width/2.5,
              //     child: SocialLoginButton(
              //         label: 'Google',
              //         onPressed: (){
              //           // Provider.of<UserController>(context,listen: false).googleRegister();
              //         },
              //         icon: 'assets/google.png'),
              //   ),
              // ],),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34.0),
                child: Divider(color: Colors.black,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 26.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Нет аккаунта?',),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Registration()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Регистрация',style: TextStyle(color: SugarLifeTheme.green),),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}

// class Adas extends StatelessWidget
// {
//   @override
//   Widget build(BuildContext context) {
//     return WebView(
//       navigationDelegate: (route){
//         // print(route.url.contains('/id/result//success'));
//         // return false;
//       },
//
//     );
//   }
// }
