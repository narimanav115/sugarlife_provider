import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/fields_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/user_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/profile/authorization.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:validators/validators.dart';



class Registration extends StatefulWidget {
  final FieldsController controller = FieldsController();
  @override
  _RegistrationState createState() => _RegistrationState();

}

class _RegistrationState extends State<Registration> {
  @override
  Widget build(BuildContext context) {
    if(Provider.of<UserController>(context).repository.isAuthenticated){
      Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
    }
    bool err = true;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: SugarAppbar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              RouteNameBar(label: 'Регистрация'),
              CustomTextField(
                hintText: 'Имя',
                onChanged: (e){
                  widget.controller.name = e;
                  setState(() {
                    widget.controller.nameCheck(widget.controller.name);
                  });
                  },
                headText: widget.controller.nameError,
              ),
              CustomTextField(
                hintText: 'Фамилия',
                onChanged: (e){
                  widget.controller.surname = e;
                  setState(() {
                    widget.controller.surnameCheck(widget.controller.surname);
                  });
                  },
                headText: widget.controller.surnameError,
              ),
              CustomTextField(
                hintText: 'Email',
                keyBoard: TextInputType.emailAddress,
                onChanged: (e){
                  widget.controller.email = e;
                  setState(() {
                    widget.controller.emailCheck(widget.controller.email);
                  });
                  },
                headText: widget.controller.emailError,
              ),
              CustomTextField(
                hintText: 'Телефон',
                keyBoard: TextInputType.phone,
                onChanged: (e){
                  widget.controller.phone = e;
                  setState(() {
                    widget.controller.checkNumber(widget.controller.phone);
                  });
                },
                headText: widget.controller.phoneError
                ,
              ),
              // CustomTextField(hintText: 'Страна', onChanged: (e){print('  ');}, headText: '',),
              // CustomTextField(hintText: 'Город', onChanged: (e){print('  ');}, headText: '',),
              CustomTextField(
                hintText: 'Пароль',
                keyBoard: TextInputType.visiblePassword,
                obscure: true,

                onChanged: (e){
                  widget.controller.password = e;
                  setState(() {
                    widget.controller.passwordLength(widget.controller.password);
                  });
                  },
                headText: widget.controller.passwordError,
              ),
              CustomTextField(
                obscure: true,
                keyBoard: TextInputType.visiblePassword,
                hintText: 'Подтверждение пароля',
                onChanged: (e){
                  widget.controller.confPassword = e;
                  setState(() {
                    err = widget.controller.validateRegister();
                  });
                  },
                headText: widget.controller.confPassError,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34.0),
                child: Text('Пароль должен содержать не меньше 6 символов, содержать цифры изаглавные буквы и не должен быть идентичен почте и имени'),
              ),
              ActionButton(duration:1000, label: 'Зарегистрироваться', onPressed: () async{
                    UserModel user = UserModel(
                        name: widget.controller.name,
                        surname: widget.controller.surname,
                        phone: widget.controller.phone,
                        email: widget.controller.email,
                        password: widget.controller.password);
                    if(
                    await Provider
                        .of<UserController>(context, listen: false)
                        .registration(user)
                    ){
                      Navigator.push(context, MaterialPageRoute(builder: (_)=>HomePage()));
                      showAlertDialog(context, '', 'Успешно зарегистрирован');
                    }else{
                      showAlertDialog(context, 'Ошибка', 'Проверьте вводимые данные');
                    }
                }
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34.0),
                child: Text('Регистрируясь, вы соглашаетесь с пользовательским соглашением и политикой конфеденциальности '),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34.0),
                child: Divider(color: Colors.black,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 26.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Уже есть аккаунт?'),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Authorization()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Вход',style: TextStyle(color: SugarLifeTheme.green),),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16 ,)

            ],
          ),
        )
    );
  }
}


class RegistrationForms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextField(hintText: 'Имя', onChanged: (e){}, headText: '',),
        CustomTextField(hintText: 'Фамилия', onChanged: (e){print('  ');}, headText: '',),
        CustomTextField(hintText: 'Email', onChanged: (e){print('  ');}, headText: '',),
        CustomTextField(hintText: 'Телефон', onChanged: (e){print('  ');}, headText: '',),
        CustomTextField(hintText: 'Страна', onChanged: (e){print('  ');}, headText: '',),
        CustomTextField(hintText: 'Город', onChanged: (e){print('  ');}, headText: '',),
        CustomTextField(hintText: 'Пароль', onChanged: (e){print('  ');}, headText: '',),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 34.0),
          child: Text('Пароль должен содержать не меньше 6 символов, содержать цифры изаглавные буквы и не должен быть идентичен почте и имени'),
        )
      ],
    );
  }
}
