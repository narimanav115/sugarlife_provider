import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/secondary_pages/profile/address_add.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class Addresses extends StatefulWidget {

  @override
  _AddressesState createState() => _AddressesState();
}

class _AddressesState extends State<Addresses> {
  @override
  Widget build(BuildContext context) {
    List<AddressModel> addresses = Provider.of<UserController>(context).repository.addresses;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
          child: Column(
            children: [
              RouteNameBar(label: 'Мои адреса'),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, i) {
                    return ListTile(
                      leading: Checkbox(
                        checkColor: Colors.white,
                        fillColor: MaterialStateProperty.all(SugarLifeTheme.green),
                        value: addresses[i].check,
                        onChanged: (bool newValue){
                          setState(() {
                            addresses[i].check = !addresses[i].check;
                            Provider.of<UserController>(context, listen: false).checkAddress(addresses[i].id);
                          });

                        },
                      ),
                      title: Text('(${addresses[i].country==null?'-':addresses[i].country})${addresses[i].city}, ${addresses[i].street} ${addresses[i].homeNumber}'),
                      subtitle: Text(addresses[i].floor==null
                          ?'${addresses[i].flatNumber} кв/дом'
                          : '${addresses[i].floor} этаж, ${addresses[i].flatNumber} квартира'),
                      trailing: GestureDetector(
                          child: Icon(Icons.settings_applications_sharp,color: SugarLifeTheme.green,),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>AddressPage(label:'Редактировать адрес', address: addresses[i],)));
                          },
                      ),
                    );
                  },
                itemCount: Provider.of<UserController>(context).repository.addresses.length,
              ),
              ActionButton(label: 'Добавить адрес',
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>AddressPage(label: 'Добавление адреса')));
                  }
                )
            ],
          )
      ),
      // bottomNavigationBar: bottomNav!=null?bottomNav:Container(),
    );
  }
}
