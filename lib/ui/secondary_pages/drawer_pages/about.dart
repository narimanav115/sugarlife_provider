import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';

class AboutPage extends StatefulWidget {
  final String type;
  AboutPage({this.type});
  @override
  _AboutPageState createState() => new _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {

  InAppWebViewController webView;
  var data = '';
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double zoom = 1.0;
    switch(widget.type){
        case 'about':
          data = Provider.of<ShoppingController>(context).res;
          zoom = 4.0;
          break;
      case 'conf':
          data = Provider.of<ShoppingController>(context).conf;
          break;
      case 'cert':
          data = Provider.of<ShoppingController>(context).cert;
          break;
      default: data = '';
    }
    return  Scaffold(
      backgroundColor: Colors.white,
        appBar: SugarAppbar(
          isCartPage: true,
        ),
        body: SafeArea(

            // child: Container(
            //   child: Center(
          child: InAppWebView(
            initialData: InAppWebViewInitialData(data: data),
            initialOptions: InAppWebViewGroupOptions(

              crossPlatform: InAppWebViewOptions(
                supportZoom: true,
                disableHorizontalScroll: true,
                  minimumFontSize: 14
              ),
              ios: IOSInAppWebViewOptions(
                pageZoom: zoom,

              )
            ),
          ),
              // ),),
        //   ],
        // ),
            ),

    );
  }
}