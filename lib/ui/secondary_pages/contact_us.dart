import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

// ignore: must_be_immutable
class ContactUs extends StatelessWidget {
  String email ='';
  String topic='';
  String desc='';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
        child:
        Column(
          children: [
            RouteNameBar(label: 'Написать нам',),
            CustomTextField(
                hintText: 'Ваш Email',
                onChanged: (e){
                  email = e;
                }),
            CustomTextField(
                hintText: 'Тема обращения',
                onChanged: (e){
                  topic = e;
                }),
            CustomTextField(
                hintText: 'Сообщение',
                maxLine: 10,
                onChanged: (e){
                  desc = e;
                }),

            ActionButton(duration:1000,label: 'Отправить', onPressed: () async{
              if(
              await Provider.of<UserController>(context,listen: false).feedback(email,topic,desc)
              ){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
                showAlertDialog(context, 'Благодарим за отклик', 'Наши менеджеры свяжутся с Вами в ближайшее время');
              }else{
                showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте правильность введенных данных');
              }
            })
          ],
        ),
      ),
    );
  }
}
