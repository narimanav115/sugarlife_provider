import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/product_container.dart';
import 'package:sugarlife/ui/widgets/scaffold_body.dart';

class StocksPage extends StatelessWidget {
  final List<Product> products;
  StocksPage({this.products});
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return ScaffoldConstructor(
        children: [
      RouteNameBar(label: 'Товары по акциям'),

          Flexible(

            child:
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 8, right: 8),
                child:GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: (w / 2 - 20) / 285,
                  crossAxisCount: 2
              ),
              itemBuilder: (context, i) {
                if(products.isNotEmpty){
                  return ProductContainer(isMainPage:false, product: products[i],);
                }
                  return Text(
                      'Товаров по акции нет'
                  );
              },
              itemCount: products!=null?products.length:1,
            ),
          ),
        ),
    ]);
  }
}
