import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/const_strings.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/address_model.dart';
import 'package:sugarlife/repositories/models/card_model.dart';
import 'package:sugarlife/repositories/models/user_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/delivery_option_page.dart';
import 'package:sugarlife/ui/secondary_pages/new_card_page.dart';
import 'package:sugarlife/ui/secondary_pages/payment_option_page.dart';
import 'package:sugarlife/ui/secondary_pages/profile/address_add.dart';
import 'package:sugarlife/ui/secondary_pages/profile/my_addresses.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/cart_item.dart';
import 'package:sugarlife/ui/widgets/unauthorized_block.dart';

class FinalOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CardModel selectedCard =
        Provider.of<UserController>(context).repository.selectedCard;
    dynamic delivery =
        Provider.of<UserController>(context).repository.deliveryPrice != null
            ? Provider.of<UserController>(context).repository.deliveryPrice
            : 0;
    dynamic totalSum =
        Provider.of<UserController>(context).repository.calculatedSum;
    UserModel user = Provider.of<UserController>(context).repository.user;
    return Scaffold(
      extendBody: true,
      appBar: SugarAppbar(isCartPage: true),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              RouteNameBar(label: 'Оформление заказа'),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {},
                    child: CartItem(
                      product:
                          Provider.of<ShoppingController>(context, listen: false)
                              .repository
                              .cartMap
                              .entries
                              .toList()[i]
                              .key,
                      number:
                          Provider.of<ShoppingController>(context, listen: false)
                              .repository
                              .cartMap
                              .entries
                              .toList()[i]
                              .value,
                    ),
                  );
                },
                itemCount: Provider.of<ShoppingController>(context)
                    .repository
                    .cartMap
                    .entries
                    .length,
                physics: NeverScrollableScrollPhysics(),
              ),
              // UnauthorizedBlock(visible: !Provider.of<UserController>(context,listen: false).repository.isAuthenticated,text: kUnauthorizedFinalCard,),
              Visibility(
                  visible: true,
                  child: Column(
                    children: [
                      FinalForms(
                        user: user != null ? user : null,
                      ),
                      DeliveryForm(delivery: delivery),
                      Visibility(
                          visible: Provider.of<UserController>(context)
                              .repository
                              .cards
                              .isNotEmpty,
                          child: CardForm()),
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 32, vertical: 16),
                        decoration: CartItem.decoration,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Сумма заказа'),
                                    Text(
                                        '${Provider.of<ShoppingController>(context).repository.totalSum}'),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Доставка'),
                                    Provider.of<ShoppingController>(context)
                                            .repository
                                            .deliveryOption
                                        ? Text(delivery.toString())
                                        : Text('0'),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Сумма к оплате'),
                                    Text((totalSum).toString()),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
              Visibility(
                visible: Provider.of<ShoppingController>(context)
                    .repository
                    .addresses
                    .isEmpty,
                child: Center(
                  child: Text('Добавьте адрес для оформления заказа'),
                ),
              ),
              // Visibility(
              //   visible: !Provider.of<UserController>(context,listen: false).repository.isAuthenticated,
              //   child: Center(child: Text('Пожалуйста зарегистируйтесь или войдите для оформления заказа'),),
              // ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Visibility(
        visible: true,
        child: Container(
          color: Colors.transparent,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 30),
          child: ActionButton(
            padding: EdgeInsets.zero,
            label: 'Подтвердить заказ',
            onPressed: () async {
              showProgressIndicator(context);
              if (Provider.of<UserController>(context, listen: false)
                  .getAuthenticated()) {
                if (await Provider.of<UserController>(context, listen: false)
                    .finalOrder()) {
                  if (selectedCard == null) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddNewCard(
                                  save: false,
                                )));
                  } else if (await Provider.of<UserController>(context,
                          listen: false)
                      .payToken()) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                    showAlertDialog(context, '', 'Заказ в обработке');
                  }
                } else {
                  showAlertDialog(context, '', 'Ошибка');
                }
              } else {
                if (await Provider.of<UserController>(context, listen: false)
                    .orderWithoutAuth()) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddNewCard(
                                save: false,
                              )));
                } else {
                  showAlertDialog(context, '', 'Ошибка');
                }
              }
            },
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }
}

class CardForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<CardModel> cards =
        Provider.of<UserController>(context).repository.cards;
    CardModel selectedCard =
        Provider.of<UserController>(context).repository.selectedCard;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 16, 0, 6),
          child: Text('Способ оплаты'),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 32),
          decoration: CartItem.decoration,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (selectedCard != null) Text('Картой'),
                      if (selectedCard != null)
                        Row(
                          children: [
                            Image.asset('assets/circles.png'),
                            Text(selectedCard.cardFirstSix +
                                '******' +
                                selectedCard.cardLastFour),
                          ],
                        ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    showModalBottomSheet<void>(
                      isScrollControlled: true,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30),
                        ),
                      ),
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          height: 577,
                          color: Colors.transparent,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 28,
                                  margin: EdgeInsets.only(top: 23, right: 30),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        child: Icon(
                                          Icons.clear,
                                          size: 28,
                                        ),
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 25),
                                  height: 280,
                                  child: SingleChildScrollView(
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder: (context, i) {
                                        return SugarStyledTile(
                                            text: cards[i].cardFirstSix +
                                                '******' +
                                                cards[i].cardLastFour,
                                            onTap: () {
                                              Provider.of<UserController>(
                                                      context,
                                                      listen: false)
                                                  .selectCard(cards[i].id);
                                              Navigator.pop(context);
                                            });
                                      },
                                      itemCount: cards.length,
                                    ),
                                  ),
                                ),
                              ]),
                        );
                      },
                    );
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=>PaymentOption()));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Выбрать карту',
                            style: TextStyle(
                                color: SugarLifeTheme.green,
                                decoration: TextDecoration.underline)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

// ignore: must_be_immutable
class FinalForms extends StatelessWidget {
  UserModel user;

  FinalForms({this.user});

  @override
  Widget build(BuildContext context) {
    String name = '';
    String surname = '';
    String email = '';
    String phone = '';
    if (user != null) {
      print('user ok');
    } else {
      user = Provider.of<UserController>(context).repository.tempUser;
    }
    return Column(
      children: [
        CustomTextField(
            hintText: '${user.name}',
            onChanged: (e) {
              name = e;
            }),
        CustomTextField(
            hintText: '${user.surname}',
            onChanged: (e) {
              surname = e;
            }),
        CustomTextField(
            keyBoard: TextInputType.emailAddress,
            hintText: '${user.email}',
            onChanged: (e) {
              email = e;
            }),
        CustomTextField(
            keyBoard: TextInputType.phone,
            hintText: '${user.phone}',
            onChanged: (e) {
              phone = e;
            }),
        Visibility(
          visible:
              !Provider.of<UserController>(context).repository.isAuthenticated,
          child: ActionButton(
              label: 'Сохранить данные',
              onPressed: () {
                Provider.of<UserController>(context, listen: false)
                        .repository
                        .tempUser =
                    UserModel(
                        name: name,
                        surname: surname,
                        email: email,
                        phone: phone);
              }),
        )
      ],
    );
  }
}

// ignore: must_be_immutable
class DeliveryForm extends StatelessWidget {
  dynamic delivery;
  AddressModel address;

  DeliveryForm({this.delivery});

  @override
  Widget build(BuildContext context) {
    if (Provider.of<ShoppingController>(context)
        .repository
        .addresses
        .isNotEmpty) {
      address = Provider.of<ShoppingController>(context)
          .repository
          .addresses
          .firstWhere((element) => element.check = true);
    }
    String pickupData =
        Provider.of<UserController>(context).repository.pickupDate != ''
            ? Provider.of<UserController>(context).repository.pickupDate +
                ', ' +
                Provider.of<UserController>(context).repository.pickupTime
            : "Выберите тип доставки";
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 16, 0, 6),
          child: Text('Способ доставки'),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 32),
          decoration: CartItem.decoration,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Visibility(
                  visible: !Provider.of<UserController>(context)
                      .repository
                      .deliveryOption,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Cамовывоз\n${Provider.of<UserController>(context).repository.pickupDate} \n ${Provider.of<UserController>(context).repository.pickupTime}'),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility(
                          visible: Provider.of<UserController>(context)
                                  .repository
                                  .isAuthenticated &&
                              address == null &&
                              Provider.of<UserController>(context)
                                  .repository
                                  .deliveryOption,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddressPage(
                                          label: 'Добавление адреса')));
                            },
                            child: Text(
                              'Добавьте адрес',
                              style: TextStyle(color: SugarLifeTheme.green),
                            ),
                          )),
                      Visibility(
                        visible: address != null &&
                            Provider.of<UserController>(context)
                                .repository
                                .deliveryOption,
                        child: address != null
                            ? Text(
                                'Доставка\nг.${address.city},\n ул. ${address.street}, д.\n${address.homeNumber},\n кв. ${address.flatNumber}')
                            : SizedBox(),
                      ),
                      Visibility(
                        visible: Provider.of<UserController>(context)
                                .repository
                                .tempAddress !=
                            null,
                        child: Provider.of<UserController>(context)
                                    .repository
                                    .tempAddress !=
                                null
                            ? Text(
                                'Доставка\nг.${Provider.of<UserController>(context).repository.tempAddress.city},\n ул. ${Provider.of<UserController>(context).repository.tempAddress.street}, д.\n${Provider.of<UserController>(context).repository.tempAddress.homeNumber},\n кв. ${Provider.of<UserController>(context).repository.tempAddress.flatNumber}')
                            : SizedBox(),
                      ),
                    ],
                  ),
                ),
                //TODO время
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //
                //     children: [
                //       Text('23.02.2021 18:00-21:00'),
                //     ],),
                // ),
                Visibility(
                  visible: true,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                            onTap: () {
                              if (address != null) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DeliveryOption(
                                              address: address,
                                            )));
                              } else {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DeliveryOption()));
                              }
                            },
                            child: Text(
                              'Изменить',
                              style: TextStyle(
                                  color: SugarLifeTheme.green,
                                  decoration: TextDecoration.underline),
                            )),
                        Visibility(
                          visible: !Provider.of<UserController>(context)
                              .repository
                              .isAuthenticated,
                          child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Addresses()));
                              },
                              child: Text(
                                'Выбрать адрес',
                                style: TextStyle(
                                    color: SugarLifeTheme.green,
                                    decoration: TextDecoration.underline),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
