import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/sugar_life_icons.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/product_container.dart';
import 'package:sugarlife/ui/widgets/tab_bar.dart';

class ProductPage extends StatelessWidget {
  final Product selectedProd;
  ProductPage({this.selectedProd});
  @override
  Widget build(BuildContext context) {
    bool hasDiscount = false;
    String discountValue = '';
    if (selectedProd.discountPrice!=null&&selectedProd.discountPrice>1){
      discountValue  = (((selectedProd.price - selectedProd.discountPrice)/selectedProd.price)*100).round().toString();
      hasDiscount = true;
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
          child: Column(
            children: [
              Visibility(
                  visible: Provider.of<ShoppingController>(context).repository.selectedProd==null,
                  child: CenterLoadingCircle()
              ),
              Visibility(
                  visible: Provider.of<ShoppingController>(context).repository.selectedProd!=null,
                  child: Column(
                    children: [
                      RouteNameBar(label: '${selectedProd.name}'),
                      BigSizeContainer(product: selectedProd,),
                      Provider.of<ShoppingController>(context).inCart(selectedProd)?
                      ActionButton(
                        label: 'Добавлено в корзину', onPressed: null,
                      )
                          :
                      ActionButton(label: 'Добавить в корзину',icon: SugarIcons.shop_cart, onPressed: (){
                        Provider.of<ShoppingController>(context,listen: false).addToCart(selectedProd);
                      }),
                      CategoriesTabs(selectedProd: selectedProd,),
                    ],
                  )
              ),
            ],
          )
      ),
    );
  }
}
