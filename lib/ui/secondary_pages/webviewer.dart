import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';

class Secure3dsPage extends StatefulWidget {
  Map<String, dynamic> body;
  bool save;
  Secure3dsPage({this.body,this.save=true});
  @override
  _Secure3dsPageState createState() => _Secure3dsPageState();
}
class _Secure3dsPageState extends State<Secure3dsPage> {
  InAppWebViewController webView;
  @override
  Widget build(BuildContext context) {
    var data =
        '<form name="downloadForm" action="${widget.body['AcsUrl']}" method="POST">'
        '<input type="hidden" name="PaReq" value=${widget.body["PaReq"]}>'
        '<input type="hidden" name="MD" value="${widget.body["MD"]}">'
        '<meta http-equiv="Authorization" content="Bearer ${Provider.of<UserController>(context).repository.token}"/>'
        '<input type="hidden" name="TermUrl" value="${widget.body["TermUrl"]}">'
        '</form>'
        '<script>'
        'window.onload = submitForm;'
        'function submitForm() { downloadForm.submit(); }'
        '</script>';
    //TODO SAVE CARD
    // showAlertDialog(context, title, desc)
    return Scaffold(
      appBar: SugarAppbar(),
      body: SafeArea(
        // child: Container(
        //   child: Center(
        child: InAppWebView(
          initialData: InAppWebViewInitialData(data: data),
          onWebViewCreated: (InAppWebViewController controller) {
            webView = controller;
          },
          // onLoadStart: (InAppWebViewController controller, Uri url) {
          //  setState(() {
          //    webView = controller;
          //    if(webView.getUrl().toString().contains('https://sugarlife')){
          //    Provider.of<UserController>(context).getCards();
          //    Navigator.pop(context);
          //    }
          //  });
          // },
          onLoadStop: (InAppWebViewController controller, Uri url) async {
            setState(() {
              if(url.toString().contains('https://sugarlife')){
                print(url);
                Provider.of<UserController>(context,listen: false).getCards();
                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
                showAlertDialog(context, '', widget.save?'Карта успешно добавлена':'Заказ оплачен и в обработке');
              }
            });
          },
        ),
        //4
        // ),),
        //   ],
        // ),
      ),
    );
  }
}
