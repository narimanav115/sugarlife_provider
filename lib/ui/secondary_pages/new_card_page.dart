import 'dart:async';
import 'package:cloudpayments/cloudpayments.dart';
import 'package:cloudpayments/cryptogram.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/const.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/webviewer.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class AddNewCard extends StatefulWidget {
  String termUrl;
  String route;
  bool save;
  String cardNumber = '';
  String month = '';
  String year = '';
  String cvv = '';
  String fullName = '';
  AddNewCard({this.save=true});
  @override
  _AddNewCardState createState() => _AddNewCardState();
}
//4
class _AddNewCardState extends State<AddNewCard> {
  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    if(!widget.save){
     if (Provider.of<UserController>(context).repository.isAuthenticated) {
       widget.route = 'save_card=FALSE&&user_id=${Provider.of<UserController>(
           context, listen: false)
           .getUserId()}&&post3ds2=TRUE&&order_id=${Provider
           .of<UserController>(context)
           .lastOrderId}';
       widget.termUrl = 'https://sugarlife.a-lux.dev/api/post3ds?save_card=FALSE&&user_id=${Provider.of<UserController>(context,listen: false).getUserId()}&&post3ds2=TRUE&&order_id=${Provider.of<UserController>(context).lastOrderId}';
     }else{
       widget.termUrl = 'https://sugarlife.a-lux.dev/api/post3ds?save_card=FALSE&&user_id=null&&post3ds2=TRUE&&order_id=${Provider.of<UserController>(context).lastOrderId}';
       widget.route = 'save_card=FALSE&&post3ds2=TRUE&&user_id=null&&order_id=${Provider.of<UserController>(context).lastOrderId}';
     }
    }else{
      widget.route = 'save_card=TRUE&&post3ds2=FALSE&&user_id=${Provider.of<UserController>(context,listen: false).getUserId()}';
      widget.termUrl = widget.termUrl = 'https://sugarlife.a-lux.dev/api/post3ds?save_card=TRUE&&user_id=${Provider.of<UserController>(context,listen: false).getUserId()}&&post3ds2=FALSE';
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SugarAppbar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RouteNameBar(label: widget.save?'Добавление карты':'Оплатите картой'),
            SizedBox(height: 16,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Text('Номер карты', textAlign: TextAlign.start,),
            ),
            CustomTextField(
                  maxLength: 16,
                  keyBoard: TextInputType.number,
                  hintText: 'XXXX XXXX XXXX XXXX',
                  onChanged: (e){
                    widget.cardNumber = e;
                    if(widget.cardNumber.length==16){
                      node.nextFocus();
                    }
                  },
                ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 34, vertical: 8 ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Срок действия'),
                  Text('CVV'),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 34),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: 30,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            hintText: 'MM',
                          ),
                          onChanged: (e){
                            widget.month = e;
                            if(widget.month.length==2){
                              node.nextFocus();
                            }
                          },

                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 34),
                        width: 30,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            hintText: 'YY',
                          ),
                          onChanged: (e){
                            widget.year = e;
                            if(widget.year.length==2){
                              node.nextFocus();
                            }
                          },

                        ),
                      ),

                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 34),
                    width: 50,
                    child: TextFormField(
                      maxLength: 3,
                      obscureText: true,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        hintText: 'CVV',
                        counterText: '',
                      ),
                      onChanged: (e){
                          widget.cvv = e;
                      },

                    ),
                  ),

                ],
              ),
            ),
            Container(

              child: CustomTextField(
                  hintText: 'Имя держателя',
                  onChanged: (e){
                    widget.fullName = e;
                  }
              ),
            ),

            Center(child: ActionButton(label: widget.save?'Добавить карту':'Оплатить', onPressed: ()async{
              Cryptogram crypto = await cryptogram(cardNumber: widget.cardNumber,expireDate: '${widget.month}/${widget.year}',cvcCode: widget.cvv, publicId: publicId);
              Map<String, dynamic> response = await Provider.of<UserController>(context,listen: false).addCard(save: widget.save, crypto: crypto.getCryptogramString(), name: widget.fullName, url: widget.route);
              showProgressIndicator(context);
              if(response['status']==500){

                showAlertDialog(context, 'Ошибка сервера', '');
              }
              // print( crypto.getCryptogramString());
              //   print(response['success']);
              //   print('pareq'+response['url']['PaReq']);
              //   print('MD'+response['url']['TransactionId'].toString());
              //   print('url'+response['url']['AcsUrl']);
              if(response['code']==0){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
                
                showAlertDialog(context, '', widget.save?'Карта успешно добавлена':'Заказ оплачен и в обработке');
              }
              if(
                response['success']=='false'||response['success']==false
              ){
                var body = {
                  'PaReq':response['url']['PaReq'],
                  'MD':response['url']['TransactionId'],
                  'TermUrl': widget.termUrl,
                  'AcsUrl':response['url']['AcsUrl'],
                };
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Secure3dsPage(body: body,save: widget.save,)));
                print(body);

              }else if(response['success']=='success'){
                showAlertDialog(context, 'Ошибка сервера', '');
                Future.delayed(Duration(milliseconds: 1000)).then((value) => Navigator.pop(context));
                Navigator.pop(context);
              } else{
                showAlertDialog(context, 'Ошибка сервера', '');
                Future.delayed(Duration(milliseconds: 1000)).then((value) => Navigator.pop(context));
                Navigator.pop(context);
              }
            }))
          ],
        ),
      ),
    );
  }
}

Future<Cryptogram> cryptogram({String cardNumber, String expireDate,String cvcCode,String publicId})async{
  return await Cloudpayments.cardCryptogram(cardNumber: cardNumber, cardDate: expireDate,cardCVC: cvcCode,publicId: publicId);
}