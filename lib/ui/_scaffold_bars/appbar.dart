
part of 'bars.dart';
@immutable
class SugarAppbar extends StatelessWidget implements PreferredSizeWidget{
  final isCartPage;

  const SugarAppbar({Key key, this.isCartPage=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('Appbar rebuild');
    return AppBar(
      automaticallyImplyLeading: true,
      // leading: IconButton(
      //   icon:  Icon(isOpen?Icons.clear:Icons.menu),
      //   onPressed: (){
      //     print('pressed');
      //     uiController.leadingTapNotifier(isOpen);
      //   },
      // ),
      title: Container(
          height: 46,
          child: Image.asset('assets/title.png')),
      centerTitle: true,
      actions: [
        isCartPage?SizedBox():GestureDetector(

          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>CartScreen()));
          },
          child: Padding(
            padding: EdgeInsets.only(right: 14, top: 2),
            child: Center(
              child: Badge(
                  badgeColor: Colors.black,
                  badgeContent: Row(
                    children: [
                      Text('${Provider.of<ShoppingController>(context).repository.totalItems}',
                        style: TextStyle(color: Colors.white, ),textAlign: TextAlign.start,),
                    ],
                  ),
                  child: Icon(SugarIcons.shop_cart) ),
            ),
          ),
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}


class DrawerAppBar extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return AppBar(
       leading: IconButton(
         icon: Icon(Icons.close),
         onPressed: (){
           Navigator.of(context).pop();
         },
       ),
      title: Image.asset('assets/title.png'),
      bottom: BlackLine(),
      centerTitle: true,
      actions: [
        Padding(
          padding: EdgeInsets.only(right: 14, top: 2),
          child: Center(
            child: Badge(
                badgeColor: Colors.black,
                badgeContent: Row(
                  children: [
                    Text('${Provider.of<ShoppingController>(context).repository.totalItems}',
                      style: TextStyle(color: Colors.white, ),textAlign: TextAlign.start,),
                  ],
                ),
                child: Icon(SugarIcons.shop_cart) ),
          ),
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class BlackLine extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 3,
      color: Colors.black,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(10);
}
