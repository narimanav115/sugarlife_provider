part of 'bars.dart';

class DrawerCustom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String res = parse(Provider.of<ShoppingController>(context).res).outerHtml;
    double topAppPadding = MediaQuery.of(context).padding.top + kToolbarHeight + 1;
    return Container(
      padding: EdgeInsets.only(top: topAppPadding),
      width: MediaQuery.of(context).size.width,
      child: Drawer(
        child: Column(
          children: [
            SugarStyledTile(
                icon: SugarIcons.info_circle, 
                text: 'О нас',
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>AboutPage(type: 'about',)));
                  // showAboutDialog(
                  //   context: context,
                  //   applicationName: 'O нас',
                  //   applicationLegalese: about,
                  // );
                },
            ),
            SugarStyledTile(
                image: 'assets/policy.png',
                scale: 4.0,
                text: 'Политика конфиденциальности',
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>AboutPage(type: 'conf',)));
                },
            ),
            SugarStyledTile(
                icon: SugarIcons.assignment,
                text: 'Договор оферты',
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>AboutPage(type: 'cert',)));
                },
            ),
            Visibility(
              visible: Provider.of<UserController>(context).repository.isAuthenticated,
              child: SugarStyledTile(image: 'assets/exit.png', scale: 3.0, text: 'Выход',
                onTap: (){
                    Provider.of<UserController>(context,listen: false).unAuthorize();
                    Navigator.pop(context);
                },
              ),
            ),
            UnauthorizedBlock(
                visible: !Provider.of<ShoppingController>(context,listen: false).repository.isAuthenticated,
            )
          ]
        ),
      ),
    );
  }
}

class SugarStyledTile extends StatelessWidget {
  final IconData icon;
  final image;
  final String text;
  final endText;
  final onTap;
  final double scale;

  const SugarStyledTile({Key key,this.icon,@required this.text, this.image, this.endText,@required this.onTap, this.scale=1.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      onLongPress: onTap,
      onDoubleTap: onTap,
      onSecondaryTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Column(
        children: [
          Divider(height: 1),
          Container(
            height: 62,

            padding: EdgeInsets.only(left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(children: [
                  if(image!=null)Image.asset(image, scale: scale,),
                  if(icon!=null)Icon(icon, color: SugarLifeTheme.green,),
                  SizedBox(width: 13,),
                  Text(text),

                ],),
                if(endText!=null)Container(
                  padding: EdgeInsets.only(right: 16),
                  child: Row(
                    children: [
                      Text(endText),
                    ],
                  ),
                )else Container(
                    padding: EdgeInsets.only(right: 16),
                    child: Row(
                      children: [
                        Text(''),
                      ],
                    ),
                ),
              ],
            ),
          ),
          Divider(height: 1,),
        ],
      ),
    );
  }
}
void showAboutDialog({
  BuildContext context,
  var applicationName,
  var applicationLegalese,
}) {
  assert(context != null);
  showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AboutDialog(
        children: [
          Text(applicationLegalese)
        ],
        applicationName: applicationName,

      );
    },
  );
}
var about = 'Sugar Life - совершенно уникальный продукт в области сахарной депиляции!'
    'На сегодняшний день компания представляет широкую линейку профессионального продукта - сахарной пасты для депиляции и полную серию по уходу за кожей. Вся продукция изготавливается на собственном производстве по новейшим технологиям, прошедшая все клинические исследования и имеющая все сертификаты качества и декларации соответствия. Компания стремительно растет, более 50 городов имеет представителя нашей компании мы обслуживаем не только люксовые салоны, но и обучающие центры.'
'Ассортимент Sugar life - это линия средств как для профессионалов, которые предпочитают более мягкие виды паст так и для начинающих мастеров в области депиляции . Отдельно специально разработанная линейка для домашнего использования. Ароматная Spa линия из 8 видов так же не оставит равнодушным. Наши преимущества - собственное производство, обучающий центр с опытными технологами - преподавателями. Продукт созданный по уникальной технологии с учётом всех потребностей в области депиляции. Соотношение цены и качества продукции - это одно из главных преимуществ компании. Слоган компании Sugar life - сладкая жизнь начинается с Нас! Вот так в 2013 году и появилась идея создания первого производства в Казахстане сахарной пасты для депиляции.';
