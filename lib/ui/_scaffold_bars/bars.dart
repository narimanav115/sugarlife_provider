import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/features/navigation_bloc/navigation_bloc.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';

import 'package:sugarlife/theme/styles.dart';

import 'package:sugarlife/ui/secondary_pages/cart_page.dart';
import 'package:sugarlife/sugar_life_icons.dart';
import 'package:html/parser.dart' show parse;
import 'package:sugarlife/ui/secondary_pages/drawer_pages/about.dart';
import 'package:sugarlife/ui/widgets/unauthorized_block.dart';

part 'appbar.dart';
part 'navbar.dart';
part 'route_name_bar.dart';
part 'drawer.dart';