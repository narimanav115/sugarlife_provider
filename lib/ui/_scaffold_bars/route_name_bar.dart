part of 'bars.dart';

class RouteNameBar extends StatelessWidget {
  final String label;

  const RouteNameBar({Key key,@required this.label}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: SugarLifeTheme.grey,
      child: SizedBox(
        height: 57,
        width: MediaQuery.of(context).size.width,
        child: Center(child: Text(label, style: SugarLifeTheme.regular,)),
      ),
    );
  }
}
