
part of 'bars.dart';
class SugarNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationBloc, UIState>(
      builder: (context, snapshot) {
        return BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          // selectedLabelStyle: TextStyle(e),
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(SugarIcons.home),
              // ignore: deprecated_member_use
              title: Text('Главная',
                maxLines: 2,),
            ),
            BottomNavigationBarItem(
              icon: Icon(SugarIcons.categories),
              // ignore: deprecated_member_use
              title: Text('Категории',
                  maxLines: 2 ),
            ),
            BottomNavigationBarItem(
              icon: Icon(SugarIcons.email),

              // ignore: deprecated_member_use
              title: Text('Поддержка',
                  textAlign: TextAlign.center,
                  maxLines: 2),
            ),
            BottomNavigationBarItem(
              icon: Icon(SugarIcons.profile),
              // ignore: deprecated_member_use
              title: Text('Профиль',
                  maxLines: 2),
            ),
          ],
          currentIndex: snapshot.index,
          selectedItemColor: Colors.black,
          iconSize: 28,
          unselectedFontSize: 10,
          selectedFontSize: 10,
          showUnselectedLabels: true,
          unselectedItemColor: Colors.grey,
          // backgroundColor: Colors.blue,
          onTap: (index){
            BlocProvider.of<NavigationBloc>(context).add(UpdateIndex(index));
            // uiController.changeIndex(index);
          },
        );
      }
    );
  }
}

