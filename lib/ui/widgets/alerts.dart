import 'package:flutter/material.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/secondary_pages/profile/authorization.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';

showAlertDialog(BuildContext context, String title, String desc) {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK", style: TextStyle(color: Colors.white),),
    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(SugarLifeTheme.green)),
    onPressed: () { Navigator.pop(context);},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(desc),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showProgressIndicator(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return GestureDetector(
        onTap: (){
          Navigator.pop(context);
        },
        child: Center(
          child: Container(
            height: 40,
            width: 40,
              child: LoadingCircle(color: Colors.green,),
          ),
        ),
      );
    },
  ).then((exit) {
    if (exit == null) return;
    if (exit) {
      // user pressed Yes button4
    } else {
      // user pressed No button
    }
  });
}
showAuthDialog(BuildContext context, String title, String desc) {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK", style: TextStyle(color: Colors.white),),
    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.teal)),
    onPressed: () { Navigator.pop(context);},
  );
  Widget authButton = TextButton(
    child: Text("Авторизоваться", style: TextStyle(color: Colors.white),),
    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(SugarLifeTheme.green)),
    onPressed: () {
       Navigator.push(context,MaterialPageRoute(builder: (context)=>Authorization()));
      },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(desc),
    actions: [
      authButton,
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
