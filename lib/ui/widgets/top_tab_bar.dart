import 'package:category_picker/category_picker.dart';
import 'package:category_picker/category_picker_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
class TopTabBar extends StatefulWidget {
  final Function onValueChanged;
  TopTabBar({this.onValueChanged});

  @override
  _TopTabBarState createState() => _TopTabBarState();
}

class _TopTabBarState extends State<TopTabBar> {
  @override
  Widget build(BuildContext context) {
    List<CategoryPickerItem> items = Provider.of<ShoppingController>(context).repository.categoryWidgets;
    return Container(
      margin: EdgeInsets.only(left: 0.0, top: 16),
      child: CategoryPicker(
        unselectedItemBorderColor: Color(0xffFFE24B),
        itemBorderRadius: BorderRadius.zero,
        selectedItemBorderColor: Color(0xffFFE24B),
        selectedItemColor: Colors.white,
        unselectedItemColor: Color(0xffFFE24B),
        selectedItemTextLightThemeColor: Colors.black,
        itemHeight: 50,
        items: items,
        onValueChanged:(value) {

          widget.onValueChanged(value);
          setState(() {

          });
          },
      ),
    );
  }
}
