import 'package:flutter/material.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';

class SocialLoginButton extends StatefulWidget {
  final String label;
  final Function onPressed;
  final icon;

  const SocialLoginButton(
      {Key key,
      @required this.label,
      @required this.onPressed,
      @required this.icon})
      : super(key: key);
  @override
  _SocialLoginButtonState createState() => _SocialLoginButtonState();
}

class _SocialLoginButtonState extends State<SocialLoginButton> {
  bool loading = false;
  double width = 200;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) => change());
  }

  change() {
    setState(() {
      width = context.size.width;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(accentColor: SugarLifeTheme.green),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: GestureDetector(
          onTap: () async {
            setState(() {
              loading = true;
            });
            widget.onPressed();
            await Future.delayed(Duration(milliseconds: 400));

            setState(() {
              loading = false;
            });
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.black),
            ),
            width: MediaQuery.of(context).size.width - 50,
            height: 50,
            child: Center(
              child: !loading
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (widget.icon != null) Image.asset(widget.icon),
                        Container(
                            width: width * 0.5,
                            child: Text(
                              widget.label,
                              style: TextStyle(color: Colors.black),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            )),
                      ],
                    )
                  : LoadingCircle(color: SugarLifeTheme.green),
            ),
          ),
        ),
      ),
    );
  }
}
