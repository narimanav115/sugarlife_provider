import 'package:flutter/material.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';

class ActionButton extends StatefulWidget {
  final duration;
  final String label;
  final double fontSize;
  final Function onPressed;
  final IconData icon;
  final Icon iconIcon;
  final backgroundColor;
  final padding;
  final color;

  const ActionButton(
      {Key key,
      @required this.label,
      @required this.onPressed,
        this.duration = 400,
      this.icon,
        this.iconIcon,
        this.color,
      this.backgroundColor = Colors.transparent,
      this.padding = const EdgeInsets.symmetric(vertical: 8.0),
      this.fontSize = 14})
      : super(key: key);
  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  bool loading = false;
  double width = 200;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // WidgetsBinding.instance.addPostFrameCallback((_) => change());
  }
  change() {
    setState(() {
      width = context.size.width;
    });
  }
  @override
  void dispose() {

    super.dispose();

  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding,
      child: ColoredBox(
        color: widget.backgroundColor ?? Colors.white,
        child: GestureDetector(
          onTap: () async {
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              if(widget.onPressed != null)
                loading = true;
              });
            if(widget.onPressed != null)
            widget.onPressed();
            await Future.delayed(Duration(milliseconds: widget.duration));

            setState(() {
              loading = false;
            });
          },
          child: ColoredBox(
            color: widget.color==null?SugarLifeTheme.green:widget.color,

            child: SizedBox(
              width: MediaQuery.of(context).size.width - 50,
              height: 50,
              child: Center(
                child: !loading
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (widget.icon != null)Icon(widget.icon, color: Colors.white),
                          SizedBox(
                              width: width * 0.5 + 20,
                              child: Text(
                                widget.label,
                                style: SugarLifeTheme.whiteText
                                    .copyWith(fontSize: widget.fontSize),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                              )),
                          if (widget.iconIcon != null)widget.iconIcon,
                        ],
                      )
                    : LoadingCircle(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
