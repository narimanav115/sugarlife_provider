import 'package:flutter/material.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';

class OutlinedActionButton extends StatefulWidget {
  final String label;
  final Function onPressed;

  const OutlinedActionButton(
      {Key key, @required this.label, @required this.onPressed})
      : super(key: key);
  @override
  _OutlinedActionButtonState createState() => _OutlinedActionButtonState();
}

class _OutlinedActionButtonState extends State<OutlinedActionButton> {
  bool loading = false;
  double width = 200;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) => change());
  }

  change() {
    setState(() {
      width = context.size.width;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: GestureDetector(
        onTap: () async {
          setState(() {
            loading = true;
          });
          widget.onPressed();
          await Future.delayed(Duration(milliseconds: 300));

          setState(() {
            loading = false;
          });
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 2, color: SugarLifeTheme.green),
          ),
          width: MediaQuery.of(context).size.width - 50,
          height: 50,
          child: Center(
            child: !loading
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: width * 0.7,
                          child: Text(
                            widget.label,
                            style: TextStyle(color: SugarLifeTheme.green),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                          )),
                    ],
                  )
                : LoadingCircle(
                    color: SugarLifeTheme.green,
                  ),
          ),
        ),
      ),
    );
  }
}
