import 'package:flutter/material.dart';
import 'package:sugarlife/sugar_life_icons.dart';
import 'package:sugarlife/theme/styles.dart';

class CustomTextField extends StatefulWidget {
  final hintText;
  final enabled;
  final prefixIcon;
  final onChanged;
  final obscure;
  final controller;
  final headText;
  final keyBoard;
  final maxLine;
  final initialValue;
  final Function onEditingComplete;
  final IconData icon;
  final Function onTapIcon;
  final validator;
  final maxLength;


  CustomTextField({Key key ,@required this.hintText,this.enabled=true,this.maxLength,this.validator,this.initialValue, this.onTapIcon,this.onEditingComplete, this.prefixIcon, this.obscure, @required this.onChanged,this.controller,this.headText, this.keyBoard, this.maxLine=1, this.icon});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 8),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if(widget.headText!=null) Text(widget.headText,style: TextStyle(color: SugarLifeTheme.green),),
            SizedBox(height: 8,),
            Container(
              height: 45.0+widget.maxLine*10,
              child: TextFormField(
                enabled: widget.enabled,
                validator: widget.validator,
                initialValue: widget.initialValue,
                // onSubmitted: widget.onEditingComplete,
                // controller: controller,
                cursorColor: Colors.black38,
                obscureText: widget.obscure ?? false,
                maxLength: widget.maxLength,
                maxLines: widget.maxLine,
                decoration: new InputDecoration(
                  counterText: '',
                  hintText: widget.hintText ?? '',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  suffixIcon: (widget.icon!=null)? GestureDetector(onTap: widget.onTapIcon ,child: Icon(widget.obscure? SugarIcons.obscure_true:SugarIcons.obscure_false, color: Colors.black,size: 10 )):null,
                  border: new OutlineInputBorder(
                    borderSide: new BorderSide(color:  Colors.black),
                  ),
                ),
                keyboardType:this.widget.keyBoard != null ? this.widget.keyBoard : TextInputType.name,
                style: new TextStyle(
                  color: Colors.black,
                ),
                onChanged: widget.onChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}