import 'package:flutter/material.dart';

class LoadingCircle extends StatelessWidget {
  final Color color;

  const LoadingCircle({Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(color ?? Colors.white),
      ),
    );
  }
}
