import 'package:flutter/material.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/theme/styles.dart';

class CategoriesTabs extends StatelessWidget {
  final Product selectedProd;
  CategoriesTabs({this.selectedProd});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultTabController(
          length: 3,
          child: Column(
            children: [
              Container(
                height: 50,
                child: CustomTabBar(),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Container(
                  height: 200,
                  child: TabBarView(
                    children: [
                      SingleChildScrollView(child: Text('${selectedProd.weight}${selectedProd.measureType}\n'+selectedProd.description)),
                      SingleChildScrollView(child: Text('${selectedProd.composition}')),
                      SingleChildScrollView(child: Text('${selectedProd.usage}')),

                    ],
                  ),
                ),
              )

            ],
          ),
        ),
      ],
    );
  }
}

class CustomTabBar extends StatefulWidget {
  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {

  final List<String> tabsName=['Описание','Состав','Применение'];


  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBar(
        isScrollable: true,
        onTap: (index){
        },
        unselectedLabelColor: Colors.black,
        labelColor: Colors.black,
        indicatorPadding: EdgeInsets.zero,
        labelPadding: EdgeInsets.symmetric(horizontal: 0.1),
        indicator: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(
                    0, 1), // changes position of shadow
              ),
            ],
            border: Border.all(color: SugarLifeTheme.yellow, width: 2),
            color: Colors.white),
        tabs: tabsName.map((e) =>
            Container(
              // decoration: BoxDecoration(
              //     border: Border.symmetric(vertical: BorderSide(color: Colors.white, width: 2,), horizontal: BorderSide(color: Colors.white, width: 2,))),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Center(
                      child: new Text(e)),
                ))).toList(),
      ),
    );
  }
}

