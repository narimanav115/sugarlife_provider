import 'package:flutter/material.dart';
import 'package:sugarlife/ui/secondary_pages/profile/authorization.dart';
import 'package:sugarlife/ui/secondary_pages/profile/registration.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/buttons/outlined_green_button.dart';

class UnauthorizedBlock extends StatelessWidget {
  final bool visible;
  final String text;
  const UnauthorizedBlock({
    Key key,
    this.visible,
    this.text
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Visibility(
          visible: visible,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 20.0,),
              if(text!=null && text != '')Center(
                child:
                    Column(children: [
                      Text('Вы не авторизированы'),
                      Text('$text'),
                    ]
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        child: ActionButton(label: 'Вход', onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Authorization()));
                        })),
                    SizedBox(width: 16,),
                    Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        child: OutlinedActionButton(label: 'Регистрация', onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Registration()));
                        })),
                  ],
                ),
              )
            ],
          )
      ),
    );
  }
}