import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/const.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/sugar_life_icons.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/main_pages/home.dart';
import 'package:sugarlife/ui/secondary_pages/product_page.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';

class ProductContainer extends StatelessWidget {
  final Product product;
  final Function onTap;
  final bool isMainPage;

  const ProductContainer({Key key, this.isMainPage, this.product, this.onTap})
      : super(key: key);

//   @override
//   _ProductContainerState createState() => _ProductContainerState();
// }
//
// class _ProductContainerState extends State<ProductContainer> {

  @override
  // void initState() {
  //   if (widget.product.discountPrice!=null&&widget.product.discountPrice>1){
  //     discountValue  = (((widget.product.price - widget.product.discountPrice)/widget.product.price)*100).round().toString();
  //     hasDiscount = true;
  //   }
  //   super.initState();
  // }
  @override
  Widget build(BuildContext context) {
    String discountValue = '';
    bool hasDiscount = false;
    bool noDiscount = (product.discountPrice == null);
    return Padding(
      padding: EdgeInsets.all(2.0),
      child: ColoredBox(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(0.0),
          child: GestureDetector(
            onTap: () async {
              if (await Provider.of<ShoppingController>(context, listen: false)
                  .getProductById(product.id, product.categoryId)) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductPage(
                              selectedProd:
                                  Provider.of<ShoppingController>(context)
                                      .repository
                                      .selectedProd,
                            )));
              } else {
                showAlertDialog(context, '', 'Произошла ошибку');
              }
            },
            child: Stack(
              children: [
                Flex(
                  direction: Axis.vertical,
                  children: [
                    CachedNetworkImage(
                      imageUrl: imagesUrl + '${product.image}',
                      imageBuilder: (context, imageProvider) => DecoratedBox(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fitWidth,
                            image: imageProvider,
                            // image: product.image==null
                            //     ?AssetImage('assets/product.png')
                            //     :NetworkImage(imagesUrl+'${product.image}')
                            // ,
                          ),
                        ),
                        child: SizedBox(
                          height: 140,
                          width: 140,
                        ),
                      ),
                      placeholder: (context, url) => CenterLoadingCircle(),
                      errorWidget: (context, url, error) => Image(
                        image: AssetImage('assets/product.png'),
                        height: 140,
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Text(
                            '${product.name}',
                            softWrap: true,
                            maxLines: 2,
                            semanticsLabel: '...',
                            style: SugarLifeTheme.regular,
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                    ),
                    SizedBox(
                      height: 20,
                      child: GestureDetector(
                        onTap: onTap,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text('${product.weight} ${product.measureType}')
                              // SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                      child: GestureDetector(
                        onTap: onTap,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              !isMainPage
                                  ? noDiscount
                                      ? Text(
                                          '${product.price} ₸',
                                          style: SugarLifeTheme.regularBold,
                                        )
                                      : Text(
                                          '${product.discountPrice} ₸',
                                          style: SugarLifeTheme.regularBold,
                                        )
                                  : SizedBox()
                              // SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          if (!isMainPage)
                            !Provider.of<ShoppingController>(context)
                                    .inCart(product)
                                ? ActionButton(
                                    icon: SugarIcons.shop_cart,
                                    label: 'Добавить в корзину',
                                    onPressed: () {
                                      Provider.of<ShoppingController>(context,
                                              listen: false)
                                          .addToCart(product);
                                    })
                                : ActionButton(
                                    label: 'Добавлено в корзину',
                                    onPressed: null,
                                  ),
                          if (isMainPage)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                noDiscount
                                    ? Text(
                                        '${product.price} ₸',
                                        style: SugarLifeTheme.regularBold,
                                      )
                                    : Text(
                                        '${product.discountPrice} ₸',
                                        style: SugarLifeTheme.regularBold,
                                      ),
                                GestureDetector(
                                  onTap: () {
                                    Provider.of<ShoppingController>(context,
                                            listen: false)
                                        .getProductById(
                                            product.id, product.categoryId);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ProductPage(
                                                  selectedProd: Provider.of<
                                                              ShoppingController>(
                                                          context)
                                                      .repository
                                                      .selectedProd,
                                                )));
                                  },
                                  child: Icon(Icons.arrow_forward_sharp),
                                )
                              ],
                            ),
                          if (!isMainPage) SizedBox()
                        ],
                      ),
                    )
                  ],
                ),
                Positioned(
                    top: 8,
                    left: 12,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          visible: Provider.of<UserController>(context)
                              .repository
                              .isAuthenticated,
                          child: LikeButton(
                            id: product.id,
                            isFavourite: product.isFavourite,
                          ),
                        ),
                        SizedBox(
                          width: 70,
                        ),
                        Visibility(
                          visible: hasDiscount,
                          child: Row(
                            children: [
                              DecoratedBox(

                                decoration: BoxDecoration(
                                  color: Colors.red,
                                ),
                                child: SizedBox(
                                  width: 44,
                                  height: 21,
                                  child: Align(
                                    alignment: Alignment.center ,
                                    child: Text(
                                      '$discountValue %',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BigSizeContainer extends StatelessWidget {
  final Product product;

  BigSizeContainer({this.product});

  @override
  Widget build(BuildContext context) {
    bool hasDiscount = false;
    String discountValue = '';
    if (product.discountPrice != null && product.discountPrice > 1) {
      discountValue =
          (((product.price - product.discountPrice) / product.price) * 100)
              .round()
              .toString();
      hasDiscount = true;
    }
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: GestureDetector(
          onTap: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => ProductPage(
            //           selectedProd: product,
            //         )));
          },
          child: ColoredBox(
            color: Colors.white,
            // padding: EdgeInsets.symmetric(horizontal: 16),
            child: Stack(
              children: [
                Flex(
                  direction: Axis.vertical,
                  children: [
                    SizedBox(height: 12),
                    CachedNetworkImage(
                      imageUrl: imagesUrl + '${product.image}',
                      imageBuilder: (context, imageProvider) => DecoratedBox(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fitHeight,
                            image: imageProvider,
                          ),
                        ),
                        child: SizedBox(
                          height: 200,
                          width: 200,
                        ),
                      ),
                      placeholder: (context, url) => CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.lightGreen),
                      ),
                      errorWidget: (context, url, error) => Image(
                        image: AssetImage('assets/product.png'),
                        height: 140,
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: EdgeInsets.all(0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18, vertical: 4),
                            child: SizedBox(
                              width: 220,
                              height: 50,
                              child: Text(
                                '${product.name}',
                                softWrap: true,
                                maxLines: 3,
                                textAlign: TextAlign.center,
                                style: SugarLifeTheme.regular,
                              ),
                            ),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                      ),
                    ),
                    SizedBox(
                      height: 40,
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              product.discountPrice == null
                                  ? Text(
                                      '${product.price} ₸',
                                      style: SugarLifeTheme.regularBold,
                                    )
                                  : Text(
                                      '${product.discountPrice} ₸',
                                      style: SugarLifeTheme.regularBold,
                                    ),
                              product.isAvailable
                                  ? Text('В наличии')
                                  : Text('Нет на складе')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                    top: 8,
                    left: 12,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          visible: Provider.of<UserController>(context)
                              .repository
                              .isAuthenticated,
                          child: LikeButton(
                            id: product.id,
                            isFavourite: product.isFavourite,
                          ),
                        ),
                        SizedBox(
                          width: 220,
                        ),
                        Visibility(
                          visible: hasDiscount,
                          child: Row(
                            children: [
                              Container(
                                width: 44,
                                height: 21,
                                decoration: BoxDecoration(
                                  color: Colors.red,
                                ),
                                child: Text(
                                  '$discountValue %',
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                alignment: Alignment.center,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LikeButton extends StatefulWidget {
  final int id;
  bool isFavourite;
  LikeButton({
    @required this.isFavourite,
    @required this.id,
  });


  @override
  _LikeButtonState createState() => _LikeButtonState();
}

class _LikeButtonState extends State<LikeButton> {
  _LikeButtonState();


  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      child: widget.isFavourite
          ? Icon(
        Icons.favorite_sharp,
        color: Color(0xFF59B439),
        size: 30,
      )
          : Icon(
        Icons.favorite_outline,
        color: Color(0xFF59B439),
        size: 30,
      ),
      onTap: () {
        setState(() {
          widget.isFavourite = !widget.isFavourite;
          // Provider.of<UserController>(context, listen:false ).repository.productList.firstWhere((element) => element.id==widget.id);
        });
        Provider.of<UserController>(context, listen: false)
            .updateFavorite(widget.id);
      },
    );
  }
}
