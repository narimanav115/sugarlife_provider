import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/repositories/controllers/const.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/main_pages/home.dart';

class CartItem extends StatelessWidget {
  final Product product;
  final int quantity;
  final int number;
  final addVisible;
  CartItem({this.quantity = -22, this.product,this.number,this.addVisible=true});
  static BoxDecoration decoration = BoxDecoration(
    color: Colors.white,
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.2),
        blurRadius: 4,
        spreadRadius: 3,
        offset: Offset(0, 0), // changes position of shadow
      ),
    ],
  );
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16.0),
      child: SizedBox(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: DecoratedBox(
            decoration: decoration,
            child: SizedBox(
              height: 118.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: EdgeInsets.all(1),
                      child: CachedNetworkImage(
                        imageUrl: imagesUrl+product.image,
                        imageBuilder: (context, imageProvider) =>
                            DecoratedBox(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.fitWidth,
                                  image: imageProvider,
                                  // image: product.image==null
                                  //     ?AssetImage('assets/product.png')
                                  //     :NetworkImage('${product.image}')
                                  // ,
                                ),
                              ),
                              child: SizedBox(
                                height: 80,
                                width: 80,
                              ),
                            ),
                        placeholder: (context,url) => CenterLoadingCircle(),
                        errorWidget: (context, url, error) => Image(image: AssetImage('assets/product.png'),height: 140,),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*0.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            quantity>=0
                                ? '${product.name}, ${product.weight} ${product.measureType}\n'+'$quantity шт'
                                : '${product.name}, ${product.weight} ${product.measureType}',
                            softWrap: true,
                            maxLines: 4,
                            style: SugarLifeTheme.regular.copyWith(fontSize: 14),
                          ),
                          SizedBox(height: 20,),
                          Text(product.discountPrice==null?product.price.toString()+'₸':product.discountPrice.toString()+'₸', style: SugarLifeTheme.regularBold,)
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: SizedBox(
                      // padding: EdgeInsets.only(right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            child: Icon(Icons.cancel, color: Colors.lightGreen,),
                            onTap: (){
                              Provider.of<ShoppingController>(context,listen: false).removeAllProducts(product);
                            },
                            behavior: HitTestBehavior.deferToChild,
                          ),
                          Visibility(
                            visible: addVisible,
                            child: SizedBox(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  // IconButton(
                                  //   icon: Icon(Icons.remove, color: Colors.grey,),
                                  //   onPressed: (){
                                  //     Provider.of<ShoppingController>(context,listen: false).removeFromCart(product);
                                  //   },
                                  //   padding: EdgeInsets.all(0),
                                  //   alignment: Alignment.centerRight,
                                  // ),
                                  GestureDetector(
                                    child: SizedBox(
                                        width: 30,
                                        height: 30,
                                        child: Icon(Icons.remove, color: Colors.grey,size: 30,),
                                    ),
                                    onTap: (){
                                      Provider.of<ShoppingController>(context,listen: false).removeFromCart(product);
                                    },
                                  ),
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black)
                                    ),
                                    child: SizedBox(
                                      width: 25,
                                      height: 25,

                                        child: Center(child: Text('$number', textAlign: TextAlign.center,style: TextStyle(fontSize: 14),)),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: GestureDetector(
                                      onTap: (){
                                        Provider.of<ShoppingController>(context,listen: false).addToCart(product);
                                      },
                                        child: Icon(Icons.add, color: Colors.lightGreen,size: 30,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


}

class OrderItem extends StatelessWidget {
  final String weight;
  final String productImage;
  final String product;
  final price;
  final int quantity;
  final int number;
  final String measureType;
  final addVisible;
  OrderItem({
    this.measureType,
    this.weight,this.productImage,this.price,this.quantity = -22, this.product,this.number,this.addVisible=true});
  static BoxDecoration decoration = BoxDecoration(
    color: Colors.transparent,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
        },
        child: Container(
          height: 100.0,
          decoration: decoration,
          child: Row(
            children: [
              Container(
                child: Flex(
                  direction: Axis.horizontal,
                  children: [
                    CachedNetworkImage(
                    imageUrl: imagesUrl+productImage,
                    imageBuilder: (context, imageProvider) =>
                        DecoratedBox(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: imageProvider,
                              // image: product.image==null
                              //     ?AssetImage('assets/product.png')
                              //     :NetworkImage('${product.image}')
                              // ,
                            ),
                          ),
                          child: SizedBox(
                          height: 100,
                          width: 100
                          ,

                          ),
                        ),
                    placeholder: (context,url) => CenterLoadingCircle(),
                    errorWidget: (context, url, error) => Image(image: AssetImage('assets/product.png'),height: 140,),
                  ),
                    // Container(
                    //   margin: EdgeInsets.only(right: 10),
                    //   height: 54,
                    //   width:  54,
                    //   decoration: BoxDecoration(
                    //     image: DecorationImage(
                    //       image: AssetImage(
                    //         'assets/product.png',
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    SizedBox(width: 10,),
                    Container(
                      width: MediaQuery.of(context).size.width*0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '$product, $weight $measureType',
                            softWrap: true,
                            maxLines: 3,
                            style: SugarLifeTheme.regular.copyWith(fontSize: 14),
                          ),
                          if(quantity>=0)Text(
                              '$quantity шт',
                            style: TextStyle(fontSize: 12),
                          ),
                          Text('$price ₸', style: SugarLifeTheme.regularBold,)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}


