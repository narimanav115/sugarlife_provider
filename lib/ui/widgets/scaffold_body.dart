import 'package:flutter/material.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';

class ScaffoldConstructor extends StatelessWidget {
  final List<Widget> children;
  final Color colour;
  const ScaffoldConstructor({Key key, @required this.children,this.colour=SugarLifeTheme.shadowGrey}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SugarLifeTheme.shadowGrey,
      appBar: SugarAppbar(),
      body:  Column(
          children: children,
      ),
    );
  }
}
