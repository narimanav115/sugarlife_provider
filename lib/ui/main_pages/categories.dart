part of 'home.dart';
class _CategoriesPage extends StatelessWidget {
  final scrollController = ScrollController();


//   @override
//   __CategoriesPageState createState() => __CategoriesPageState();
// }
//
// class __CategoriesPageState extends State<_CategoriesPage> {
  // ScrollController controller = ScrollController();
  IconData priceIcon = Icons.arrow_upward_sharp;
  IconData massIcon = Icons.arrow_downward_sharp;
  bool filter = false;
  String property = '';
  int direction = 0;
  List<Product> productsList = [];
  int categoryId = -1;
  int index = 1;
  var padding = const SizedBox(height: 16,);

  @override
  Widget build(BuildContext context) {
    void setupScrollController() {
      scrollController.addListener(() {
        if (scrollController.position.atEdge) {
          if (scrollController.position.pixels != 0) {
            index +=1;
            if(!filter){
              Provider.of<ShoppingController>(context,listen: false).addProducts(page: index,categoryId: categoryId, );
            }else{
              Provider.of<ShoppingController>(context,listen: false).addSortProducts(property: property,direction: direction,page: index);
            }
            Future.delayed(Duration(seconds: 1));
          }
        }
      });
    }
    setupScrollController();
    // productsList = Provider.of<ShoppingController>(context).getMainPageProductsList();
    // if(Provider.of<ShoppingController>(context).getProductsList().isNotEmpty){
    productsList = Provider.of<ShoppingController>(context).repository.productList;
  // }
    print('category page');
    double w = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child: TopTabBar(
            onValueChanged: (value){
              filter = false;
              if(value.id<0){
                index  = 1;
                Provider.of<ShoppingController>(context, listen: false).getAllProducts();
              }else{
                index  = 1;
                Provider.of<ShoppingController>(context, listen: false).getProducts(value.id);
              }

              categoryId = value.id;
            },
          ),
        ),
        Visibility(
          visible: categoryId<0,
          child: Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: (){
                    filter = true;
                    if(priceIcon == Icons.arrow_upward_sharp){
                      priceIcon = Icons.arrow_downward_sharp;
                      Provider.of<ShoppingController>(context,listen: false).sort(property: 'price',direction: 1);
                      property = 'price';
                      direction = 1;
                      if(categoryId>0){
                        Provider.of<ShoppingController>(context, listen: false).getProducts(categoryId);
                      }
                    }else{
                      property = 'price';
                      direction = 0;
                      priceIcon = Icons.arrow_upward_sharp;
                      Provider.of<ShoppingController>(context,listen: false).sort(property: 'price',direction: 0);
                      if(categoryId>0){
                        Provider.of<ShoppingController>(context, listen: false).getProducts(categoryId);
                      }
                    }

                  },
                  child: Row(
                    children: [
                      Icon(priceIcon,color: SugarLifeTheme.green),
                      SizedBox(width: 5,),
                      Text('По цене',style: TextStyle(fontSize: 15),),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                      filter = true;
                      if(massIcon == Icons.arrow_upward_sharp){
                        property = 'massa';
                        direction = 1;
                        massIcon = Icons.arrow_downward_sharp;
                        Provider.of<ShoppingController>(context,listen: false).sort(property: 'massa',direction: 1);
                        if(categoryId>0){
                          Provider.of<ShoppingController>(context, listen: false).getProducts(categoryId);
                        }
                      }else{
                        property = 'price';
                        direction = 0;
                        massIcon = Icons.arrow_upward_sharp;
                        Provider.of<ShoppingController>(context,listen: false).sort(property: 'massa',direction: 0);
                        if(categoryId>0){
                          Provider.of<ShoppingController>(context, listen: false).getProducts(categoryId);
                        }
                      }
                  },
                  child: Row(
                    children: [
                      Icon(massIcon,color: SugarLifeTheme.green),
                      SizedBox(width: 5,),
                      Text('По массе', style: TextStyle(fontSize: 15),),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.symmetric(horizontal: 16),
        //   child: Container(
        //     margin: EdgeInsets.only(top:10),
        //     child: Column(
        //       children: [
        //
                Provider.of<ShoppingController>(context).repository.selectedIsUploaded
                          && Provider.of<ShoppingController>(context).repository.productList.isNotEmpty
                    ?
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, left: 8, right: 8),
                      child: GridView.builder(
                              controller: scrollController,
                              shrinkWrap: false,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                  childAspectRatio: (w / 2 - 20) / 285,
                                  crossAxisCount: 2
                              ),
                              itemBuilder: (context, i) {
                                return ProductContainer(
                                  isMainPage: false,
                                  product: productsList[i],
                                );
                              },
                              itemCount: productsList.length
                              ,
                            ),
                    ),
                  )
                    :Center(child: CenterLoadingCircle()),




          //     ]
          //   ),
          // ),

        // ),

      ],
    );
  }
}

// class Sort extends StatefulWidget {
//   @override
//   _SortState createState() => _SortState();
// }
//
// class _SortState extends State<Sort> {
//
//   @override
//   Widget build(BuildContext context) {
//     return ;
//   }
// }

class CenterLoadingCircle extends StatelessWidget {
  const CenterLoadingCircle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 20),
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightGreen),
        ),
      ),
    );
  }
}
