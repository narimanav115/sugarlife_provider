part of 'home.dart';


class _SupportPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String email ='';
    String topic='';
    String desc='';
    print('support page');
    return SingleChildScrollView(
      child: Column(
        children: [
          RouteNameBar(label: 'Написать нам',),
          CustomTextField(
              hintText: 'Ваш Email',
              onChanged: (e){
                email = e;
          }),
          CustomTextField(
              hintText: 'Тема обращения',
              onChanged: (e){
                topic = e;
          }),
          CustomTextField(
              hintText: 'Сообщение',
              maxLine: 10,
              onChanged: (e){
                desc = e;
          }),

          ActionButton(duration:1500,label: 'Отправить', onPressed: () async{
            if(
            await Provider.of<UserController>(context,listen: false).feedback(email,topic,desc)
            ){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
              showAlertDialog(context, 'Благодарим за отклик', 'Наши менеджеры свяжутся с Вами в ближайшее время');
            }else{
              showAlertDialog(context, 'Произошла ошибка', 'Пожалуйста проверьте правильность введенных данных');
            }
          })
        ],
      ),
    );
  }
}
