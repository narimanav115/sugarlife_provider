part of 'home.dart';


class _ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('profile page');
    return Container(
      child: Column(
        children:[
          Visibility(
          visible: Provider.of<UserController>(context).repository.isAuthenticated,
          child: Column(
            children: [
              RouteNameBar(label: 'Мой профиль'),
              SugarStyledTile(icon: SugarIcons.profile, text: 'Персональная информация',
                  onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalInfo()));
              }),
              SugarStyledTile(icon: SugarIcons.Location, text: 'Адреса доставки',
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Addresses()));
                },
              ),
              SugarStyledTile(image: 'assets/paycard.png',scale: 3.0, text: 'Платежные карты', onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>PaymentOption()));
              },),
              SugarStyledTile(image: 'assets/orders.png',scale: 4.0, text: 'Мои заказы', onTap: (){
                Provider.of<UserController>(context,listen: false).getOrdersHistory();
                Navigator.push(context, MaterialPageRoute(builder: (context)=>OrderHistory()));
              },),
              SugarStyledTile(image: 'assets/heart.png',scale: 4.0, text: 'Избранное', onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Favourites()));
              },),
              SugarStyledTile(image: 'assets/change_password.png',scale: 4.0, text: 'Сменить пароль', onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ChangePassword()  ));
              },),
            ],
          ),
        ),
          UnauthorizedBlock(
            visible: !Provider.of<UserController>(context,listen: false).repository.isAuthenticated,
            text: kUnauthorizedProfile,
          )
        ]
      ),
    );
  }
}

