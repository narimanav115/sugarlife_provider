import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sugarlife/const_strings.dart';
import 'package:sugarlife/features/navigation_bloc/navigation_bloc.dart';
import 'package:sugarlife/repositories/controllers/const.dart';
import 'package:sugarlife/repositories/controllers/shopping_controller.dart';
import 'package:sugarlife/repositories/controllers/user_controller.dart';
import 'package:sugarlife/repositories/models/product_model.dart';
import 'package:sugarlife/sugar_life_icons.dart';
import 'package:sugarlife/theme/styles.dart';
import 'package:sugarlife/ui/_scaffold_bars/bars.dart';
import 'package:sugarlife/ui/secondary_pages/articles_webview.dart';
import 'package:sugarlife/ui/secondary_pages/contact_us.dart';
import 'package:sugarlife/ui/secondary_pages/payment_option_page.dart';
import 'package:sugarlife/ui/secondary_pages/profile/favourites.dart';
import 'package:sugarlife/ui/secondary_pages/profile/my_addresses.dart';
import 'package:sugarlife/ui/secondary_pages/profile/order_history.dart';
import 'package:sugarlife/ui/secondary_pages/profile/password_change.dart';
import 'package:sugarlife/ui/secondary_pages/profile/personal_info.dart';
import 'package:sugarlife/ui/secondary_pages/stocks.dart';
import 'package:sugarlife/ui/widgets/InputField.dart';
import 'package:sugarlife/ui/widgets/buttons/action_button.dart';
import 'package:sugarlife/ui/widgets/loading_indicator.dart';
import 'package:sugarlife/ui/widgets/product_container_variations/product_container.dart';
import 'package:sugarlife/ui/widgets/top_tab_bar.dart';
import 'package:sugarlife/ui/widgets/unauthorized_block.dart';
import 'package:sugarlife/ui/widgets/alerts.dart';
import 'package:http/http.dart' as http;
part 'categories.dart';
part 'profile.dart';
part 'support.dart';

class HomePage extends StatelessWidget {
  final String route;
  HomePage({this.route = ''});


  String id = '/home';
  final List<Widget> _buildBodyPages =[
    _HomePageBody(),
    _CategoriesPage(),
    _SupportPage(),
    _ProfilePage(),];

  @override
  Widget build(BuildContext context) {

    print('parent');
    return BlocProvider(
      create:(_)=> NavigationBloc(),
      child: Scaffold(
        drawerScrimColor: Colors.transparent,
        appBar: SugarAppbar(),
        drawer: DrawerCustom(),
        body: BlocBuilder<NavigationBloc, UIState>(
          builder: (context,  state) {
            return IndexedStack(
            index: state.index,
            children: _buildBodyPages,
            );
          }
        ),
        bottomNavigationBar: SugarNavBar(),
      ),
    );
  }
}

class _HomePageBody extends StatelessWidget {
  final Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<ShoppingController>(context, listen: true).repository.productList;
    var slides = Provider.of<ShoppingController>(context).repository.slides;
    Set<Marker> markers = Provider.of<ShoppingController>(context).repository.markers;
    var companyAddresses = Provider.of<ShoppingController>(context).repository.companyAddresses;
    var companyContacts = Provider.of<ShoppingController>(context).repository.companyContacts;
    // var categories = Provider.of<ShoppingController>(context).repository.categoriesList;
    var articles = Provider.of<ShoppingController>(context).repository.articles;
    var stocks = Provider.of<ShoppingController>(context).repository.stocks;
    companyAddresses.forEach((element) {
      markers.add(Marker(markerId: MarkerId('${element.id}'), position: LatLng(element.latitude, element.longitude)));
    });

    CameraPosition newCameraPosition;
    if(companyAddresses.isNotEmpty){
      newCameraPosition = CameraPosition(
          target: LatLng(companyAddresses[0].latitude,companyAddresses[0].longitude,)
          ,zoom: 11
      );
    }
    print('homebody');
    double w = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Column(
          children: [
            Provider.of<ShoppingController>(context).repository.slides.isNotEmpty
                ?
            CarouselSlider.builder(
              options: CarouselOptions(
                height: 470,
                aspectRatio: 16/9,
                viewportFraction: 1,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                scrollDirection: Axis.horizontal,
              ),
              itemCount: slides.length,
              itemBuilder: (context,i) =>
                  Stack(
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: CachedNetworkImage(
                          imageUrl: '$imagesUrl${slides[i].image}',
                          imageBuilder: (context, imageProvider) =>
                              Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fitWidth,
                                    scale: 0.9,
                                    image: imageProvider,
                                    // image: product.image==null
                                    //     ?AssetImage('assets/product.png')
                                    //     :NetworkImage('${product.image}')
                                    // ,
                                  ),
                                ),
                              ),
                          placeholder: (context,url) => CenterLoadingCircle(),
                          errorWidget: (context, url, error) => Image(image: AssetImage('assets/img1.png')),
                        ),
                      ),
                      // child: Container(
                      //     child: Image.asset('assets/img1.png', fit: BoxFit.fitWidth,scale: 0.9,))),
                      Positioned(
                          top: 40,
                          left: 16,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 50),
                                child: Text('Успейте приобрести'.toUpperCase() ,maxLines:4,style: SugarLifeTheme.bigStyle,),
                              ),
                              Padding(padding: const EdgeInsets.only(right: 100),child: Text('${slides[i].text}', style: SugarLifeTheme.regular,)),
                              Container(
                                padding: EdgeInsets.only(top: 250),
                                width: 250,
                                child: ActionButton(
                                  iconIcon: Icon(Icons.chevron_right_outlined, color: Colors.white,) ,
                                  label: 'Перейти к покупкам',
                                  onPressed: (){
                                    BlocProvider.of<NavigationBloc>(context).add(UpdateIndex(1));
                                  },
                                  backgroundColor: Colors.transparent,
                                ),
                              ),
                            ],
                          )
                      )
                    ],
                  ),
            )
            :CenterLoadingCircle(),
            Container(
              height: 75,
              child: Center(
                child: Text('КАТАЛОГ', style: SugarLifeTheme.bigStyle,),
              ),
            ),
            // DefaultTabController(
            //     length: 3,
            //     child: CustomTabBar()),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                margin: EdgeInsets.only(top:10),
                child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0),
                        child: TopTabBar(
                          onValueChanged: (value){
                            if(value.id<0){
                              Provider.of<ShoppingController>(context, listen: false).getAllProducts();
                            }else{
                              Provider.of<ShoppingController>(context, listen: false).getProducts(value.id);
                            }
                          },
                        ),
                      ),
                      Visibility(
                          visible: Provider.of<ShoppingController>(context).repository.productList.isEmpty,
                          child: Center(child: CenterLoadingCircle())
                      ),

                      Visibility(
                          visible: !Provider.of<ShoppingController>(context).repository.selectedIsUploaded,
                          child: CenterLoadingCircle()
                      ),

                      Visibility(
                        visible: Provider.of<ShoppingController>(context).repository.selectedIsUploaded
                            && Provider.of<ShoppingController>(context).repository.productList.isNotEmpty
                        ,
                        // child: Container(margin: EdgeInsets.all(20),child: Center(child: Text('В данной категории пока нет товаров'),))
                        child: GridView.builder(
                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 10,
                              childAspectRatio: (w / 2 - 20) / 285,
                              crossAxisCount: 2),
                          itemBuilder: (context, i) {
                            return ProductContainer(
                              product: products[i],
                              isMainPage: true,
                            );
                          },
                          itemCount: products.length>4?4:products.length,
                          physics: NeverScrollableScrollPhysics(),
                        ),
                      )


                    ]
                ),
              ),

            ),
            // Visibility(
            //   visible: products.isNotEmpty,
            //   child: Padding(
            //     padding: const EdgeInsets.symmetric(horizontal: 16),
            //     child: GridView.builder(
            //       shrinkWrap: true,
            //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //           crossAxisSpacing: 10,
            //           mainAxisSpacing: 10,
            //           childAspectRatio: (w / 2 - 20) / 285,
            //           crossAxisCount: 2),
            //       itemBuilder: (context, i) {
            //         return ProductContainer(
            //           product: products[i],
            //           isMainPage: true,
            //         );
            //       },
            //       itemCount: 4,
            //       physics: NeverScrollableScrollPhysics(),
            //     ),
            //   ),
            // ),
            Container(
              height: 75,
              child: Center(
                child: Text('АКЦИИ', style: SugarLifeTheme.bigStyle,),
              ),
            ),
            Visibility(
              visible: stocks.isNotEmpty,
              child: ListView.builder(
                  itemCount: stocks.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, i){
                    return
                      Container(
                        child: Stack(
                          children: [
                            CachedNetworkImage(
                              imageUrl: '$imagesUrl${stocks[i].image}',
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.fitWidth,
                                        scale: 0.9,
                                        image: imageProvider,
                                        // image: product.image==null
                                        //     ?AssetImage('assets/product.png')
                                        //     :NetworkImage('${product.image}')
                                        // ,
                                      ),
                                    ),
                                  ),
                              placeholder: (context,url) => CenterLoadingCircle(),
                              errorWidget: (context, url, error) => Image(image: AssetImage('assets/sale.png')),
                            ),
                            Image.network('https://sugarlife.a-lux.dev/storage/${stocks[i].image}', scale: 0.9,),
                            Positioned(
                                top: 40,
                                left: 30,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        width: 250,
                                        child: Text('${stocks[i].name}',style: TextStyle(fontSize: 18,),maxLines: 4,)),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      width: 165,
                                      height: 60,
                                      child: ActionButton(
                                          iconIcon: Icon(Icons.chevron_right_outlined, color: Colors.white,) ,
                                          label: 'Подробнее',
                                          onPressed: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=>StocksPage(products: stocks[i].products)));
                                          },
                                        backgroundColor: Colors.transparent,
                                        ),
                                    ),
                                  ],
                                )
                            ),
                            // Align(
                            //   alignment: Alignment.bottomLeft,
                            //   child: Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: Container(
                            //       child: ActionButton(label: 'Подробнее',),
                            //     ),
                            //   ),
                            // )
                          ],
                        ),
                      );
                  }

                  ),
            ),
            Container(
              height: 75,
              child: Center(
                child: Text('СТАТЬИ', style: SugarLifeTheme.bigStyle,),
              ),
            ),
            Provider.of<ShoppingController>(context).repository.articles.isNotEmpty
                ?
            CarouselSlider.builder(
              options: CarouselOptions(
                height: 400,
                aspectRatio: 16/9,
                viewportFraction: 1,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 10),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                scrollDirection: Axis.horizontal,
              ),
              itemCount: articles.length,
              itemBuilder: (context,i) =>
                  GestureDetector(
                    // onTap: () async{
                    //   var res = await getText(articles[i].id.toString());
                    //   if(res!="error"){
                    //     Navigator.push(context, MaterialPageRoute(builder: (context)=>ArticlesWebView(data: res)));
                    //   }
                    // },
                    child: Column(
                      children: [
                        Container(
                          height:200,
                          child: CachedNetworkImage(
                              imageUrl: imagesUrl+'/${articles[i].image}',
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.fitWidth,
                                        scale: 0.9,
                                        image: imageProvider,
                                        // image: product.image==null
                                        //     ?AssetImage('assets/product.png')
                                        //     :NetworkImage('${product.image}')
                                        // ,
                                      ),
                                    ),
                                  ),
                              placeholder: (context,url) => CenterLoadingCircle(),
                              errorWidget: (context, url, error) => Image(image: AssetImage('assets/articles.png')),
                            ),
                        ),

                        // child: Container(
                        //     child: Image.asset('assets/img1.png', fit: BoxFit.fitWidth,scale: 0.9,))),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('${articles[i].name}'.toUpperCase() ,style: SugarLifeTheme.bigStyle,)),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('${articles[i].description}',
                              style: SugarLifeTheme.regular,)),
                      ],
                    ),
                  ),
            )
                :CenterLoadingCircle(),
            Container(
              height: 75,
              child: Center(
                child: Text('ПРЕДСТАВИТЕЛЬСТВА\nСНГ', style: SugarLifeTheme.bigStyle, textAlign: TextAlign.center,),
              ),
            ),
            if (companyAddresses.isNotEmpty&&markers.isNotEmpty&&newCameraPosition!=null) Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                height: 250,
                color: Colors.black12,
                child: GoogleMap(
                  mapType: MapType.normal,
                  markers: markers,
                  gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                    new Factory<OneSequenceGestureRecognizer>(
                          () => new EagerGestureRecognizer(),
                    ),
                  ].toSet(),
                  zoomGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  onCameraMove:(CameraPosition cameraPosition){
                    newCameraPosition = cameraPosition;
                  },
                  initialCameraPosition: newCameraPosition,
                  onMapCreated: (GoogleMapController mapper) {
                    _controller.complete(mapper);
                    // print('on created: ${LatLng(companyAddresses[0].latitude,companyAddresses[0].longitude,)}');
                    mapper.animateCamera(
                        CameraUpdate.newCameraPosition(newCameraPosition)
                    );
                  },
                ),
              ),
            )
            else LoadingCircle(),

            Container(
              height: 400,
              color: Color(0xFFE8F5E2),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(SugarIcons.Phone
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Звоните нам'),
                              Text('${Provider.of<ShoppingController>(context).repository.getContactsOfCompany()}'),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 16,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(SugarIcons.email_rounded
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Пишите нам'),
                              Text('${Provider.of<ShoppingController>(context).repository.getContactsOfCompany()}'),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 16,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(SugarIcons.Location
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Наши адреса:'),
                              Container(
                                  width: 90,
                                  child: Text('${Provider.of<ShoppingController>(context).repository.getAddressesOfCompany()}', maxLines:20)
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    ActionButton(label: 'Написать нам', onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ContactUs()));
                    }, padding: EdgeInsets.zero,)
                  ],
                ),
              ),
            ),
          ]
      ),
    );
  }
}

Future<String> getText(String id) async {
  print(id);
  http.Response response = await http.get(Uri.parse(baseUrl+'articles/$id'));
  if(response.statusCode == 200){
    return jsonDecode(response.body)['text'];
  }else{
    print(response.statusCode);
    return 'error';
  }
}